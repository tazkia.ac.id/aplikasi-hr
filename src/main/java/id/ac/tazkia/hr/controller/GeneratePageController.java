package id.ac.tazkia.hr.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class GeneratePageController {

	@GetMapping("/side-menu")
	public ModelAndView generatePage() {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("side-menu-template");
		return mv;
	}
	
}
