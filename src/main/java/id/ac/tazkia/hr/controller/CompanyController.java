package id.ac.tazkia.hr.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;

import id.ac.tazkia.hr.dto.BaseTree;
import id.ac.tazkia.hr.dto.BaseTreeRequest;
import id.ac.tazkia.hr.dto.CompanyRequest;
import id.ac.tazkia.hr.entity.Company;
import id.ac.tazkia.hr.entity.Job;
import id.ac.tazkia.hr.entity.JobLevel;
import id.ac.tazkia.hr.entity.Organization;
import id.ac.tazkia.hr.service.CompanyService;
import id.ac.tazkia.hr.service.JobLevelService;
import id.ac.tazkia.hr.service.JobService;
import id.ac.tazkia.hr.service.OrganizationService;

@Controller
public class CompanyController {
	private static final ObjectMapper MAPPER = new ObjectMapper();
	
	@Autowired
	private CompanyService companyService;
	
	@Autowired
	private OrganizationService orgService;
	
	@Autowired
	private JobService jobService;
	
	@Autowired
	private JobLevelService jobLevelService;
	
	@Value("${upload.path}")
	private String uploadPath;
	
	@GetMapping("/company")
	public ModelAndView company() {
	    ModelAndView mv = new ModelAndView();
	    mv.setViewName("company/company");
	    return mv;
	}

	@GetMapping("/company-profile")
	public String company(Model model) {
		Company company = this.companyService.findTopByCompanyTypeOrderByIdDesc(1);
    	if(company != null){
    		model.addAttribute("companyForm", company);
    	} else {
    		model.addAttribute("companyForm", new Company());
    	}
	    
	    return "company/company-profile";
	}
	
	@PostMapping("/company-add")
	public String addCompanyProfile(@ModelAttribute @Valid CompanyRequest companyReq, Model model, BindingResult bindResult) throws Exception {
//		if(companyReq.getLogo().isEmpty() || companyReq.getSignature().isEmpty()){
//			model.addAttribute("errorMsg", "Please select a file!");
//			if(formName.equals("headquarterAdd")){
//				return "headquarter-add";
//			} else {
//				return "branch-add";
//			}
//        }
		    	
		Map<String, MultipartFile> maps = new HashMap<String, MultipartFile>();
    	maps.put("logo", companyReq.getLogo());
    	maps.put("signature", companyReq.getSignature());
    	
    	Company company = new Company();
    	
    	Company existCompany = this.companyService.findById(UUID.fromString(companyReq.getId())).orElse(null);
    	if(existCompany != null){
    	    ObjectReader objectReader = MAPPER.readerForUpdating(existCompany);
    	    company = objectReader.readValue(MAPPER.writeValueAsString(companyReq));
    	} else {
    		company = MAPPER.convertValue(companyReq, Company.class);
    	}
    	String tempLogo = companyReq.getLogo().getOriginalFilename();
    	String tempSign = companyReq.getSignature().getOriginalFilename();
    	if(tempLogo.length() > 0)
    	company.setLogo(company.getName().concat("_logo").concat(tempLogo.substring(tempLogo.lastIndexOf("."))));
    	if(tempSign.length() > 0)
    	company.setSignature(company.getName().concat("_signature").concat(tempSign.substring(tempSign.lastIndexOf("."))));
    	
        saveUploadedFiles(maps, company);
        
        return "redirect:/company-list";
	}
	
	@GetMapping("/company-list")
	public ModelAndView companyList(Model model) {
	    ModelAndView mv = new ModelAndView();
	    mv.setViewName("company/company-list");
	    List<Company> companyList = this.companyService.findAll();
	    String parentBranch = "";
	    for(Company company : companyList){
	    	if(company.getCompanyType() == 1){
	    		parentBranch = company.getId().toString();
	    	}
	    }
	    model.addAttribute("companies", companyList);
	    model.addAttribute("parentBranch", parentBranch);
	    
	    return mv;
	}
	
	@GetMapping("/headquarter-add")
	public ModelAndView headquarterAdd(Model model, @RequestParam(value = "id", required = false) String id) throws Exception {
	    
		ModelAndView mv = new ModelAndView();
	    mv.setViewName("company/headquarter-add");
	    
	    Company company;
	    if(id == null){
	    	company = new Company();
	    	company.setId(UUID.randomUUID());
	    	company.setCompanyType(1);
	    	company.setParentBranch(company.getId());
	    	
	    } else {
	    	company = this.companyService.findById(UUID.fromString(id)).orElse(null);
	    }
	    model.addAttribute("headquarterForm", company);
//	    model.addAttribute("imagePath", uploadPath + "\\" + company.getLogo());	    
	    
	    return mv;
	}
	
	@GetMapping("/branch-add")
	public ModelAndView branchAdd(Model model, 
		@RequestParam(value = "parent_id", required = false) String parentId,
		@RequestParam(value = "id", required = false) String id) {
		
	    ModelAndView mv = new ModelAndView();
	    mv.setViewName("company/branch-add");
	    
	    Company company;
	    if(id == null){
		    company = new Company();
		    company.setId(UUID.randomUUID());
	    	company.setCompanyType(2);
	    	company.setParentBranch(UUID.fromString(parentId));
	    } else {
	    	company = this.companyService.findById(UUID.fromString(id)).orElse(null);
	    }

    	model.addAttribute("branchForm", company);
    	
	    return mv;
	}
	
	@GetMapping("/api/parent-branch-npwp")
	public ResponseEntity<?> getParentBranchNpwp(@RequestParam(value = "parent", required = false) String parentId) 
		throws Exception {
		
		Company company = this.companyService.findById(UUID.fromString(parentId)).orElse(null);
		
		return ResponseEntity.status(HttpStatus.OK).body(MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(company));
	}
	/* End of method branch */
	
	@GetMapping("/api/company-all")
	public ResponseEntity<?> findAllCompany() throws Exception {
	   
		List<Company> companyList = this.companyService.findAll();
	   
	    return ResponseEntity.status(HttpStatus.OK).body(MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(companyList));
	}
	
	/* Method for organization structure */
	@GetMapping("/organization-structure")
	public ModelAndView organizationStructure(Model model) {
	    ModelAndView mv = new ModelAndView();
	    mv.setViewName("company/organization-structure");
	    
	    return mv;
	}

	@PostMapping("/organization-add")
	public String addOrganization(@ModelAttribute @Valid BaseTreeRequest orgReq, Model model) throws Exception {
	    
		Organization org = new Organization();	
		if(orgReq.getId() != null){
			org = this.orgService.findById(UUID.fromString(orgReq.getId())).orElse(null);
		} else {
			org.setId(UUID.randomUUID());
		}
		org.setName(orgReq.getName());
	    org.setParent(orgReq.getParentId() != null ? UUID.fromString(orgReq.getParentId()) : null);
	    
		this.orgService.saveOrUpdate(org);
        
        return "redirect:/organization-structure";
	}
	
	@GetMapping("/api/organization-tree")
	public ResponseEntity<?> loadOrganizationTree() throws Exception {
	   
		List<BaseTree> orgStructures = this.orgService.loadOrganizationTree();
	   
	    return ResponseEntity.status(HttpStatus.OK).body(MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(orgStructures));
	}
	
	@GetMapping("/api/organization-all")
	public ResponseEntity<?> findAllOrganization() throws Exception {
		
		List<Organization> orgList = this.orgService.findAll();
	   
	    return ResponseEntity.status(HttpStatus.OK).body(MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(orgList));
	}

	@GetMapping("/api/organization-detail")
	public ResponseEntity<?> getOrganizationDetail(@RequestParam(value = "id", required = false) String id) throws Exception {
		Organization org = new Organization();
		if(id != null) org = this.orgService.findById(UUID.fromString(id)).orElse(null);
		
	    return ResponseEntity.status(HttpStatus.OK).body(MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(org));
	}
	
	@GetMapping("/api/organization-delete")
	public String deleteOrganization(@RequestParam(value = "id", required = false) String id) throws Exception {
	   
		Organization org = this.orgService.findById(UUID.fromString(id)).orElse(null);
		org.setIsActive(false);
	    
		this.orgService.saveOrUpdate(org);
        
        return "redirect:/organization-structure";
	}
	/* End of method organization structure */

	@GetMapping("/job-level")
	public ModelAndView jobLevel() {
	    ModelAndView mv = new ModelAndView();
	    mv.setViewName("company/job-level");
	    return mv;
	}

	@PostMapping("/job-level-add")
	public String addJobLevel(@ModelAttribute @Valid BaseTreeRequest jobLevelReq, Model model) throws Exception {
	    
		JobLevel jobLevel = MAPPER.convertValue(jobLevelReq, JobLevel.class);	
		jobLevel.setId(UUID.randomUUID());
	    
		this.jobLevelService.saveOrUpdate(jobLevel);
        
        return "redirect:/job-level";
	}
	
	@GetMapping("/api/job-level-all")
	public ResponseEntity<?> findAllJobLevel() throws Exception {
	   
		List<JobLevel> jobLevelList = this.jobLevelService.findAll();
	   
	    return ResponseEntity.status(HttpStatus.OK).body(MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(jobLevelList));
	}
	
	@GetMapping("/api/job-level-delete")
	public String deleteJobLevel(@RequestParam(value = "id", required = false) String id) throws Exception {
	   
		JobLevel jobLv = this.jobLevelService.findById(UUID.fromString(id)).orElse(null);
		jobLv.setIsActive(false);
	    
		this.jobLevelService.saveOrUpdate(jobLv);
        
        return "redirect:/job-level";
	}
	/* End of method job level */

	@GetMapping("/job-position")
	public ModelAndView jobPosition() {
	    ModelAndView mv = new ModelAndView();
	    mv.setViewName("company/job-position");
	    return mv;
	}

	@PostMapping("/job-add")
	public String addJob(@ModelAttribute @Valid BaseTreeRequest jobReq, Model model) throws Exception {
	    
		Job job = new Job();	
		if(jobReq.getId() != null){
			job = this.jobService.findById(UUID.fromString(jobReq.getId())).orElse(null);
		} else {
			job.setId(UUID.randomUUID());
		}
		job.setName(jobReq.getName());
		job.setParent(jobReq.getParentId() != null ? UUID.fromString(jobReq.getParentId()) : null);
	    
		this.jobService.saveOrUpdate(job);
        
        return "redirect:/job-position";
	}
	
	@GetMapping("/api/job-all")
	public ResponseEntity<?> findAllJob() throws Exception {
	   
		List<Job> jobList = this.jobService.findAll();
	   
	    return ResponseEntity.status(HttpStatus.OK).body(MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(jobList));
	}
	
	@GetMapping("/api/job-detail")
	public ResponseEntity<?> getJobDetail(@RequestParam(value = "id", required = false) String id) throws Exception {
		Job job = new Job();
		if(id != null) job = this.jobService.findById(UUID.fromString(id)).orElse(null);
	   
	    return ResponseEntity.status(HttpStatus.OK).body(MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(job));
	}
	
	@GetMapping("/api/job-tree")
	public ResponseEntity<?> loadJobTree() throws Exception {
	   
		List<BaseTree> jobTree = this.jobService.loadJobPositionTree();
	   
	    return ResponseEntity.status(HttpStatus.OK).body(MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(jobTree));
	}
	
	@GetMapping("/api/job-delete")
	public String deleteJob(@RequestParam(value = "id", required = false) String id) throws Exception {
	   
		Job job = this.jobService.findById(UUID.fromString(id)).orElse(null);
		job.setIsActive(false);
	    
		this.jobService.saveOrUpdate(job);
        
        return "redirect:/job-position";
	}
	/* End of method job position */
	
	/* Method for grade */
	
	/* End of method grade */
	
	/* Method for Auto Generate Format */
	@GetMapping("/auto-generate-format")
	public ModelAndView autoGenerateFormat() {
	    ModelAndView mv = new ModelAndView();
	    mv.setViewName("company/auto-generate-format");
	    return mv;
	}
	/* End of method Auto Generate Format */
	
	/* Method for My Files Category */
	
	/* End of method My Files Category */
	
	/* Method for NPP BPJS */
	@GetMapping("/npp-bpjs")
	public ModelAndView nppBpjs() {
	    ModelAndView mv = new ModelAndView();
	    mv.setViewName("company/npp-bpjs");
	    return mv;
	}
	/* End of method NPP BPJS */
	
	private void saveUploadedFiles(Map<String, MultipartFile> maps, Company company) throws IOException {
		File dirTarget = new File(uploadPath);
    	if(!dirTarget.exists()){
    		dirTarget.mkdir();
    	}
		for(Map.Entry<String, MultipartFile> map : maps.entrySet()){
	    	File fileSource;
	    	if(map.getKey().equals("logo")){
	    		fileSource = new File(uploadPath + "\\" + company.getLogo());
	    	} else{
	    		fileSource = new File(uploadPath + "\\" + company.getSignature());
	    	}
	    	
			Path path = Paths.get(fileSource.getAbsolutePath());
			Files.copy(map.getValue().getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
	    }
	    
    	this.companyService.saveOrUpdate(company);
    }
	
}
