package id.ac.tazkia.hr.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/rekrutmen")
public class RecruitmentController {
	
	@GetMapping("/")
    public String rekrutmen(Model model) {
        return "recruitmentForm";
    }

    @RequestMapping(value={"/"}, method = RequestMethod.POST)
    public String posisi(@RequestParam(name="posisi", required = true) String posisi){
        return "redirect:/" + posisi;
    }

    @GetMapping("/dosen")
    public String rekrutmenDos(Model model) {
        return "recruitmentFormDosen";
    }

    @GetMapping("/karyawan")
    public String rekrutmenKar(Model model) {
        return "recruitmentFormKaryawan";
    }
    
}
