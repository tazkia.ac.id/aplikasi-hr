package id.ac.tazkia.hr.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;

import id.ac.tazkia.hr.dto.AccountRequest;
import id.ac.tazkia.hr.entity.User;
import id.ac.tazkia.hr.service.UserService;

@Controller
public class LoginController {
	@Autowired
	private UserService userService;
	
	@RequestMapping("/user-login")
	public ModelAndView userLogin() {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("auth/login");
		
		return mv;
	}
	
	@RequestMapping("/oauth-login")
	public ModelAndView oauthLogin() {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("auth/login2");
		
		return mv;
	}
	
	@RequestMapping({"/register-user", "/reset-password"})
	public ModelAndView userRegister(Model model, @RequestParam(value = "username", required = false) String username){	
		ModelAndView mv = new ModelAndView();
		mv.setViewName("auth/user-register");
		
		model.addAttribute("paramReset", username);
	 	
		return mv;
	}
	
	@PostMapping("/registered")
	public String registered(@ModelAttribute @Valid AccountRequest accountReq, Model model) {
		ObjectMapper mapper = new ObjectMapper();
		User reqUser = mapper.convertValue(accountReq, User.class);
		User acceptedUser = userService.registerUser(reqUser);
		if(acceptedUser != null){
			return "redirect:/user-login";
		}
		model.addAttribute("errorMsg", "User not registered.");
		return "redirect:/reset-password";
	}
	
}
