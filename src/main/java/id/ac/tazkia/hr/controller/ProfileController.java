package id.ac.tazkia.hr.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ProfileController {
	@GetMapping("/profile")
    public ModelAndView employee() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("profile/profile-side");
        return mv;
    }
}
