package id.ac.tazkia.hr.controller;

import java.beans.FeatureDescriptor;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;

import id.ac.tazkia.hr.dto.EmployeeRequest;
import id.ac.tazkia.hr.entity.Employee;
import id.ac.tazkia.hr.entity.Role;
import id.ac.tazkia.hr.entity.User;
import id.ac.tazkia.hr.service.CompanyService;
import id.ac.tazkia.hr.service.EmployeeService;
import id.ac.tazkia.hr.service.JobLevelService;
import id.ac.tazkia.hr.service.JobService;
import id.ac.tazkia.hr.service.OrganizationService;

@Controller
public class EmployeeController {
	@Autowired
	private EmployeeService empService;
	
	@Autowired
	private CompanyService compService;
	
	@Autowired
	private OrganizationService orgService;
	
	@Autowired
	private JobService jobService;
	
	@Autowired
	private JobLevelService jobLvService;
	
	@GetMapping("/employee")
    public ModelAndView employee(Model model) {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("employee/employee");
        
        List<Employee> listEmployee = this.empService.findAll();
        model.addAttribute("employees", listEmployee);
        
        return mv;
    }
    
	@GetMapping("/employee-add")
    public ModelAndView employeeAdd(Model model) {
    	ModelAndView mv = new ModelAndView();
    	//mv.setViewName("employee/employee-add-side");
    	mv.setViewName("employee/employee-wizard");
    	
    	return mv;
    }
	
	@GetMapping("/employee-edit")
    public ModelAndView employeeEdit(Model model, @RequestParam(value = "id", required = false) String id) {
		ModelAndView mv = new ModelAndView();
        mv.setViewName("employee/employee-edit-general-info");
        
        Employee employee = this.empService.findById(id).orElse(null);
        model.addAttribute("employeeObj", employee);
        
        return mv;
    }
	
	@GetMapping("/employee-time-off")
    public ModelAndView timeOff(Model model, @RequestParam(value = "id", required = false) String id) {
		ModelAndView mv = new ModelAndView();
        mv.setViewName("employee/employee-time-off");
        
//        Employee employee = this.empService.findById(UUID.fromString(id)).orElse(null);
//        model.addAttribute("employeeObj", employee);
        
        return mv;
    }
	
	@GetMapping("/employee-attendance")
    public ModelAndView attendance(Model model, @RequestParam(value = "id", required = false) String id) {
		ModelAndView mv = new ModelAndView();
        mv.setViewName("employee/employee-attendance");
        
//        Employee employee = this.empService.findById(UUID.fromString(id)).orElse(null);
//        model.addAttribute("employeeObj", employee);
        
        return mv;
    }
	
	@GetMapping("/employee-overtime")
    public ModelAndView overtime(Model model, @RequestParam(value = "id", required = false) String id) {
		ModelAndView mv = new ModelAndView();
        mv.setViewName("employee/employee-overtime");
        
//        Employee employee = this.empService.findById(UUID.fromString(id)).orElse(null);
//        model.addAttribute("employeeObj", employee);
        
        return mv;
    }
	
	@GetMapping("/employee-attendance-view")
    public ModelAndView attendanceView(Model model, @RequestParam(value = "id", required = false) String id) {
		ModelAndView mv = new ModelAndView();
        mv.setViewName("employee/employee-attendance-view");
        
//        Employee employee = this.empService.findById(UUID.fromString(id)).orElse(null);
//        model.addAttribute("employeeObj", employee);
        
        return mv;
    }
	
	@GetMapping("/employee-reimburst")
    public ModelAndView reimburst(Model model, @RequestParam(value = "id", required = false) String id) {
		ModelAndView mv = new ModelAndView();
        mv.setViewName("employee/employee-reimbursment");
        
//        Employee employee = this.empService.findById(UUID.fromString(id)).orElse(null);
//        model.addAttribute("employeeObj", employee);
        
        return mv;
    }
	
	@GetMapping("/employee-loan")
	public ModelAndView loan(Model model, @RequestParam(value = "id", required = false) String id) {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("employee/employee-loan");
		
		return mv;
	}
	
	@PostMapping("/add-employee-save")
    public String addEmployeeSaved(@ModelAttribute @Valid EmployeeRequest empReq) throws Exception {
		Employee employee = new Employee();
		BeanUtils.copyProperties(empReq, employee);
		employee.setId(empReq.getEmployeeId());
		User user = new User();
		user.setId(employee.getId());
		user.setEmail(employee.getEmail());
		user.setName(employee.getFirstName().concat(employee.getLastName()));
		Role role = new Role();
		role.setId("user");
		user.setRole(role);
		employee.setUser(user);
		
		employee.setCompany(empReq.getCompanyId() != null ? this.compService.findById(UUID.fromString(empReq.getCompanyId())).orElse(null) : null);
		employee.setOrganization(empReq.getOrgId() != null ? this.orgService.findById(UUID.fromString(empReq.getOrgId())).orElse(null) : null); 
		employee.setJob(empReq.getJobId() != null ? this.jobService.findById(UUID.fromString(empReq.getJobId())).orElse(null) : null);
		employee.setJobLevel(empReq.getJobLevelId() != null ? this.jobLvService.findById(UUID.fromString(empReq.getJobLevelId())).orElse(null) : null);
		
		this.empService.saveOrUpdate(employee);
    	
		return "redirect:/employee";
    }
	
	@PostMapping("/edit-employee-save")
    public String editEmployeeSaved(@ModelAttribute @Valid EmployeeRequest empReq,  
		@RequestParam(value = "tab", required = false) String tab,
		@RequestParam(value = "emp_id", required = false) String id,
		BindingResult bindResult) throws Exception {
		Employee employee = new Employee();
		if(id != null){
			Employee existEmployee = this.empService.findById(id).orElse(null);
			if(existEmployee != null){
				BeanUtils.copyProperties(empReq, existEmployee, getNullPropertyNames(empReq));
				employee = existEmployee;
			}
		}
		if(tab.equals("employment_data")){
			employee.setOrganization(empReq.getOrgId() != null ? this.orgService.findById(UUID.fromString(empReq.getOrgId())).orElse(null) : null); 
			employee.setJob(empReq.getJobId() != null ? this.jobService.findById(UUID.fromString(empReq.getJobId())).orElse(null) : null);
			employee.setJobLevel(empReq.getJobLevelId() != null ? this.jobLvService.findById(UUID.fromString(empReq.getJobLevelId())).orElse(null) : null);
		} 
		
		this.empService.saveOrUpdate(employee);
    	
		return "redirect:/employee";
    }
	
	@GetMapping("/employee-delete")
	public String deleteEmployee(@RequestParam(value = "id", required = false) String id) throws Exception {
	   
		Employee employee = this.empService.findById(id).orElse(null);
		employee.setIsActive(false);
	    
		this.empService.saveOrUpdate(employee);
        
        return "redirect:/employee";
	}
	
	@GetMapping("/api/employee-all")
	public ResponseEntity<?> findAllEmployee() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		List<Employee> empList = this.empService.findAll();
	   
	    return ResponseEntity.status(HttpStatus.OK).body(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(empList));
	}
	
	public static String[] getNullPropertyNames(Object source) {
	    final BeanWrapper wrappedSource = new BeanWrapperImpl(source);
	    return Stream.of(wrappedSource.getPropertyDescriptors())
	        .map(FeatureDescriptor::getName)
	        .filter(propertyName -> {
	            try {
	               return wrappedSource.getPropertyValue(propertyName) == null;
	            } catch (Exception e) {
	               return false;
	            }                
	        })
	        .toArray(String[]::new);
	}

}
