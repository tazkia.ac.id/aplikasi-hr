package id.ac.tazkia.hr.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class MainController {

	@RequestMapping({"/", "/dashboard"})
	public ModelAndView dashboard() {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("dashboard-admin");
		return mv;
	}
	
}