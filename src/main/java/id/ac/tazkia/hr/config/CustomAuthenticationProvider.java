package id.ac.tazkia.hr.config;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import id.ac.tazkia.hr.entity.User;
import id.ac.tazkia.hr.service.UserService;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {
	@Autowired
	private UserService userService;
	@Autowired 
	private HttpSession session;
	
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String username = authentication.getName();
        String password = authentication.getCredentials().toString();
       
        User user = userService.findUserByEmailAndPassword(username, password);
        if(user == null){
            throw new BadCredentialsException("User not found.");
        }
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
    	grantedAuthorities.add(new SimpleGrantedAuthority(user.getRole().getName()));
    	
    	session.setAttribute("username", user.getName());
        session.setAttribute("user_image", user.getImageUrl());
        
        return new UsernamePasswordAuthenticationToken(username, password, grantedAuthorities);
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
	}

}
