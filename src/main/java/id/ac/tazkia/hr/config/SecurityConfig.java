package id.ac.tazkia.hr.config;

import java.util.ArrayList;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.user.OAuth2UserAuthority;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.thymeleaf.extras.springsecurity5.dialect.SpringSecurityDialect;

import id.ac.tazkia.hr.entity.Permission;
import id.ac.tazkia.hr.entity.User;
import id.ac.tazkia.hr.service.UserService;
import id.ac.tazkia.hr.service.impl.CustomUserService;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig {
	
	@Configuration
	@Order(1)
	public static class UserSecurityConfig extends WebSecurityConfigurerAdapter {
		@Autowired
		private CustomAuthenticationProvider authenticationProvider;
		
		public UserSecurityConfig(){
			super();
		}
		
		@Override
		protected void configure(AuthenticationManagerBuilder auth) throws Exception{
			auth.authenticationProvider(authenticationProvider);
		}
		
		@Override
	    protected void configure(HttpSecurity http) throws Exception {
	        http
	            .authorizeRequests()
	            .antMatchers("/oauth-login", "/register-user", "/registered", 
	            	"/reset-password", "/assets/**").permitAll()
	            .anyRequest().authenticated()
	            .and().formLogin()
	            .loginPage("/user-login")
	            .defaultSuccessUrl("/dashboard", true)
	            .and().logout().permitAll()
	            .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
	            .logoutSuccessUrl("/user-login")
		        .and()
	            .csrf().disable();
	    }

	}
	
	@Configuration
	public static class OauthSecurityConfig extends WebSecurityConfigurerAdapter {
		@Autowired 
		private CustomUserService customUserService;
		@Autowired 
		private UserService userService;
		@Autowired 
		private HttpSession session;
		
		public OauthSecurityConfig(){
			super();
		}
		
		@Override
	    protected void configure(HttpSecurity http) throws Exception {
	        http
	            .authorizeRequests()
	            .anyRequest().authenticated()
	            .and().oauth2Login()
	            .loginPage("/oauth-login")
	            .userInfoEndpoint()
	            .oidcUserService(customUserService)
	            .userAuthoritiesMapper(authoritiesMapper())
	            .and().defaultSuccessUrl("/dashboard", true)
	            .and().logout().permitAll()
	            .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
	            .logoutSuccessUrl("/user-login")
	            .and()
	            .csrf().disable();
	    }
		
		private GrantedAuthoritiesMapper authoritiesMapper(){
	        return (authorities) -> {
	            String emailAttrName = "email";
	            String email = authorities.stream()
	                    .filter(OAuth2UserAuthority.class::isInstance)
	                    .map(OAuth2UserAuthority.class::cast)
	                    .filter(userAuthority -> userAuthority.getAttributes().containsKey(emailAttrName))
	                    .map(userAuthority -> userAuthority.getAttributes().get(emailAttrName).toString())
	                    .findFirst()
	                    .orElse(null);
	            
	            if (email == null) {
	                return authorities;     
	            }
	            
	            User user = userService.findUserByEmail(email);
	            if(user == null) {
	                return authorities;     
	            } 

	            Set<Permission> userAuthorities = user.getRole().getPermissions();
	            if (userAuthorities.isEmpty()) {
	                return authorities;     // authorities defaultnya ROLE_USER
	            } 
	            
	            session.setAttribute("username", user.getName());
	            session.setAttribute("user_image", user.getImageUrl());
	            
	            return Stream.concat(authorities.stream(),userAuthorities.stream().map(Permission::getName)
	                .map(SimpleGrantedAuthority::new)
	                ).collect(Collectors.toCollection(ArrayList::new));	
	        };
	    }

	} 
		
	@Bean
    public SpringSecurityDialect springSecurityDialect() {
        return new SpringSecurityDialect();
    }
	
    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

}
