package id.ac.tazkia.hr.dto;

import lombok.Data;

@Data
public class BaseTree {
	private String id;
    private String name;
    private String parentId;
}
