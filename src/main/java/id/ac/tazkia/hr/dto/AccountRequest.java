package id.ac.tazkia.hr.dto;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties("confirmPassword")
public class AccountRequest {
	@NotNull
    private String email;
	
	@NotNull
    private String password;
	
    private String confirmPassword;
}
