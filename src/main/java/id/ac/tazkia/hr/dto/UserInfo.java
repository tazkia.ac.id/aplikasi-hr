package id.ac.tazkia.hr.dto;

import lombok.Data;

@Data
public class UserInfo {
    private String id;
    private String name;
    private String email;
    private String imageUrl;
    
}
