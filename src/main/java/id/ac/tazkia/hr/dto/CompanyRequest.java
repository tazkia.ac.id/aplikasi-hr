package id.ac.tazkia.hr.dto;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties({"logo", "signature"})
public class CompanyRequest {
	
    private String id;	
    private String hqInitial;
    private String name;
	private Integer companyType; 
	private String parentBranch;
	private String province;
	private String city;
	private BigDecimal umr;
	private String phone;
	private String fax;
	private String email;
    private String address;
    private String postalCode;
	private MultipartFile logo;
	private String bpjsTk;
	private String jkk; 
	private String jhtPaidBy;
	private String bpjsTkPaidBy;
	private String npwp;
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date taxableDate;
	private String taxName;
	private String taxPerson;
	private String taxPersonNpwp;
	private String kluCode;
	private MultipartFile signature;
//	private String formName;
	
}
