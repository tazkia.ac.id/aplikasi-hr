package id.ac.tazkia.hr.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties({"id", "parentId"})
public class BaseTreeRequest {
	private String id;
    private String name;
    private String parentId;
}
