package id.ac.tazkia.hr.dto;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
public class EmployeeRequest {
	
	private String employeeId;
    private String barcode;
	private String firstName;
	private String lastName;
	private String email;
	private String idCardType;
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date idCardExpiredDate;
	private String idCardNo;
	private String postalCode;
	private String citizenIdAddress;
	private String residentialIdAddress;
	private String birthPlace;
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date birthDate;
	private String mobilePhone;
	private String phone;
	private String gender;
	private String maritalStatus;
	private String bloodType;
	private String religion;
	private String companyId;
	private String orgId;
	private String jobId;
	private String jobLevelId;
	private String employmentStatus;
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date joinDate;
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date endContractDate;
	private String grade;
	private String employeeClass;
	private String schedule;
	private String approval;
	private BigDecimal salary;
	private String npwp;
	private String ptkpStatus;
	private String bankName;
	private String bankAccount;
	private String bankAccountHolder;
	private String bpjsTk;
	private String bpjsK;
	private String bpjsKf;
	private String taxConfiguration;
	private String salaryType;
	private String salaryConfiguration;
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date taxableDate;
	private String jhtConfiguration;
	private String employeeTaxStatus;
	private String jaminanPensiunConfiguration;
	private String nppBpjsTk;
	private String overtimeStatus;
	private String bpjsKConfiguration;
	private BigDecimal beginningNetto;
	private BigDecimal pph21Paid;
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date bpjsTkDate;
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date bpjsKDate;
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date jaminanPensiunDate;
	private String paymentSchedule;
	private String prorateType;
}
