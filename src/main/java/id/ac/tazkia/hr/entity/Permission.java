package id.ac.tazkia.hr.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity 
@Table(name = "s_permission") 
@Data
public class Permission {
	@Id
	@Column(name = "id")
    private String id;
	
    @Column(name = "name")
    private String name;
    
    @Column(name = "description")
    private String description;
}
