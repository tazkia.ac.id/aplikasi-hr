package id.ac.tazkia.hr.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity 
@Table(name = "s_role") 
@Data
public class Role {
	@Id
	@Column(name = "id")
    private String id;
	
	@Column(name = "name")
    private String name;
	
	@Column(name = "description")
    private String description;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
	    name = "s_role_permission",
	    joinColumns = @JoinColumn(name = "id_role"),
	    inverseJoinColumns = @JoinColumn(name = "id_permission")
    )
    private Set<Permission> permissions = new HashSet<>();
}
