package id.ac.tazkia.hr.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.Where;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Entity 
@Table(name = "employee") 
@Data
@Where(clause = "is_active = 1")
public class Employee {
	
	@Id
	@GenericGenerator(name = "sequence_emp_id", strategy = "id.ac.tazkia.hr.entity.EmployeeIdGenerator")
    @GeneratedValue(generator = "sequence_emp_id")
	@Column(name = "id")
    private String id;
	
	@Column(name = "barcode")
    private String barcode;
	
	@Column(name = "first_name")
	private String firstName;
	
	@Column(name = "last_name")
	private String lastName;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "id_card_type")
	private String idCardType;
	
	@Column(name = "id_card_expired_date")
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date idCardExpiredDate;
	
	@Column(name = "id_card_no")
	private String idCardNo;
	
	@Column(name = "postal_code")
	private String postalCode;
	
	@Column(name = "citizen_id_address")
	private String citizenIdAddress;
	
	@Column(name = "residential_id_address")
	private String residentialIdAddress;
	
	@Column(name = "birth_place")
	private String birthPlace;
	
	@Column(name = "birth_date")
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date birthDate;
	
	@Column(name = "mobile_phone")
	private String mobilePhone;
	
	@Column(name = "phone")
	private String phone;
	
	@Column(name = "gender")
	private String gender;
	
	@Column(name = "marital_status")
	private String maritalStatus;
	
	@Column(name = "blood_type")
	private String bloodType;
	
	@Column(name = "religion")
	private String religion;
	
	@ManyToOne @JoinColumn(name = "company_id")
    private Company company;
	
	@ManyToOne @JoinColumn(name = "organization_id")
    private Organization organization;
	
	@ManyToOne @JoinColumn(name = "job_id")
    private Job job;
	
	@ManyToOne @JoinColumn(name = "job_level_id")
    private JobLevel jobLevel;
	
	@Column(name = "employment_status")
	private String employmentStatus;
	
	@Column(name = "join_date")
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date joinDate;
	
	@Column(name = "end_contract_date")
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date endContractDate;
	
	@Column(name = "grade")
	private String grade;
	
	@Column(name = "employee_class")
	private String employeeClass;
	
	@Column(name = "schedule")
	private String schedule;
	
	@Column(name = "approval")
	private String approval;
	
	@Column(name = "salary")
	private BigDecimal salary;
	
	@Column(name = "npwp")
	private String npwp;
	
	@Column(name = "ptkp_status")
	private String ptkpStatus;
	
	@Column(name = "bank_name")
	private String bankName;
	
	@Column(name = "bank_account")
	private String bankAccount;
	
	@Column(name = "bank_account_holder")
	private String bankAccountHolder;
	
	@Column(name = "bpjs_tk")
	private String bpjsTk;
	
	@Column(name = "bpjs_k")
	private String bpjsK;
	
	@Column(name = "bpjs_kf")
	private String bpjsKf;
	
	@Column(name = "tax_configuration")
	private String taxConfiguration;
	
	@Column(name = "salary_type")
	private String salaryType;
	
	@Column(name = "salary_configuration")
	private String salaryConfiguration;
	
	@Column(name = "taxable_date")
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date taxableDate;
	
	@Column(name = "jht_configuration")
	private String jhtConfiguration;
	
	@Column(name = "employee_tax_status")
	private String employeeTaxStatus;
	
	@Column(name = "jaminan_pensiun_configuration")
	private String jaminanPensiunConfiguration;
	
	@Column(name = "npp_bpjs_tk")
	private String nppBpjsTk;
	
	@Column(name = "overtime_status")
	private String overtimeStatus;
	
	@Column(name = "bpjs_k_configuration")
	private String bpjsKConfiguration;
	
	@Column(name = "beginning_netto")
	private BigDecimal beginningNetto;
	
	@Column(name = "pph21_paid")
	private BigDecimal pph21Paid;
	
	@Column(name = "bpjs_tk_date")
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date bpjsTkDate;
	
	@Column(name = "bpjs_k_date")
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date bpjsKDate;
	
	@Column(name = "jaminan_pensiun_date")
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date jaminanPensiunDate;
	
	@Column(name = "payment_schedule")
	private String paymentSchedule;
	
	@Column(name = "prorate_type")
	private String prorateType;
	
	@Column(name = "is_active")
	private Boolean isActive = true;
	
	@Column(name = "created_at") @CreationTimestamp
	private Timestamp createdAt;
	
	@Column(name = "updated_at") @UpdateTimestamp
	private Timestamp updatedAt;
	
	@OneToOne(fetch = FetchType.LAZY,
        cascade =  CascadeType.ALL,
        mappedBy = "employee")
    private User user;

}
