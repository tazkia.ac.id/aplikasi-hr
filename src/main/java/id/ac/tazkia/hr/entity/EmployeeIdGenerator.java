package id.ac.tazkia.hr.entity;

import java.io.Serializable;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

public class EmployeeIdGenerator implements IdentifierGenerator {

	@Override
	public Serializable generate(SharedSessionContractImplementor session, Object obj) 
		throws HibernateException {
		Employee emp = new Employee();
		if(obj instanceof Employee)	emp = (Employee) obj;
		
		return emp.getId();
	}

}
