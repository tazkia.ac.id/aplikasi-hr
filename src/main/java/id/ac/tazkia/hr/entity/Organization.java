package id.ac.tazkia.hr.entity;

import java.sql.Timestamp;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.Where;

import lombok.Data;

@Entity 
@Table(name = "organization") 
@Data
@Where(clause = "is_active = 1")
public class Organization {
	
	@Id
	@GenericGenerator(name="uuid", strategy="uuid2")
	@GeneratedValue(generator="uuid")
	@Column(name = "id")
    private UUID id;
	
	@Column(name = "name")
    private String name;
	
	@Column(name = "parent")
    private UUID parent;
	
	@Column(name = "is_active")
	private Boolean isActive = true;
	
	@Column(name = "created_at") @CreationTimestamp
	private Timestamp createdAt;
	
	@Column(name = "updated_at") @UpdateTimestamp
	private Timestamp updatedAt;
}
