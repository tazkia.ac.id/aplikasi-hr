package id.ac.tazkia.hr.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity 
@Table(name = "s_user") 
@Data
public class User {
	@Id
	@Column(name = "id")
    private String id;
	
	@Column(name = "name")
    private String name;
	
	@Column(name = "email")
    private String email;
	
	@Column(name = "password")
    private String password;
	
	@Column(name = "image_url")
    private String imageUrl;
	
	@Column(name = "is_active")
    private Boolean isActive = true;
	
    @ManyToOne @JoinColumn(name = "id_role")
    private Role role;
    
    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "employee_id", nullable = false)
    private Employee employee;
    
}
