package id.ac.tazkia.hr.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.Where;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Entity 
@Table(name = "company") 
@Data
@Where(clause = "is_active = 1")
public class Company {
	
	@Id
	@GenericGenerator(name="uuid", strategy="uuid2")
	@GeneratedValue(generator="uuid")
	@Column(name = "id")
    private UUID id;
	
	@Column(name = "hq_initial")
    private String hqInitial;
	
	@Column(name = "name")
    private String name;
	
	@Column(name = "company_type")
	private Integer companyType;
	
	@Column(name = "parent_branch")
	private UUID parentBranch;
	
	@Column(name = "province")
	private String province;
	
	@Column(name = "city")
	private String city;
	
	@Column(name = "umr")
	private BigDecimal umr;
	
	@Column(name = "phone")
	private String phone;
	
	@Column(name = "fax")
	private String fax;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "address")
    private String address;
	
	@Column(name = "postal_code")
    private String postalCode;
	
	@Column(name = "logo")
	private String logo;
	
	@Column(name = "bpjs_tk")
	private String bpjsTk;
	
	@Column(name = "jkk")
	private String jkk; 
	
	@Column(name = "jht_paid_by")
	private String jhtPaidBy;
	
	@Column(name = "bpjs_tk_paid_by")
	private String bpjsTkPaidBy;
	
	@Column(name = "npwp")
	private String npwp;
	
	@Column(name = "taxable_date") 
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date taxableDate;
	
	@Column(name = "tax_name")
	private String taxName;
	
	@Column(name = "tax_person")
	private String taxPerson;
	
	@Column(name = "tax_person_npwp")
	private String taxPersonNpwp;
	
	@Column(name = "klu_code")
	private String kluCode;
	
	@Column(name = "signature")
	private String signature;
	
	@Column(name = "is_header_label")
	private Boolean isHeaderLabel;
	
	@Column(name = "is_active")
	private Boolean isActive = true;
	
	@Column(name = "created_at") @CreationTimestamp
	private Timestamp createdAt;
	
	@Column(name = "updated_at") @UpdateTimestamp
	private Timestamp updatedAt;

}
