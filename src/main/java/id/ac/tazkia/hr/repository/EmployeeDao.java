package id.ac.tazkia.hr.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import id.ac.tazkia.hr.entity.Employee;

public interface EmployeeDao extends JpaRepository<Employee, String> {
	Optional<Employee> findById(String id);
	List<Employee> findAll();
}
