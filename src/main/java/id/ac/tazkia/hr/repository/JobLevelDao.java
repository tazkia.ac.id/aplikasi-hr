package id.ac.tazkia.hr.repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import id.ac.tazkia.hr.entity.JobLevel;

public interface JobLevelDao extends JpaRepository<JobLevel, UUID> {
	Optional<JobLevel> findById(UUID id);
	List<JobLevel> findAll();
}
