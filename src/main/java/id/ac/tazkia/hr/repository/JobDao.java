package id.ac.tazkia.hr.repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import id.ac.tazkia.hr.entity.Job;

public interface JobDao extends JpaRepository<Job, UUID>{
	Optional<Job> findById(UUID id);
	List<Job> findAll();
}
