package id.ac.tazkia.hr.repository.data.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import id.ac.tazkia.hr.dto.BaseTree;
import id.ac.tazkia.hr.repository.data.OrganizationDataDao;

@Repository
public class OrganizationDataDaoImpl implements OrganizationDataDao {
	
	@PersistenceContext
    private EntityManager entityManager;
	
	@Override
	public List<BaseTree> loadDataTree() {
		List<BaseTree> result = new ArrayList<>();
		String sql = "select BIN_TO_UUID(org.id), org.job_name, BIN_TO_UUID(org.parent_id) " +
					 "from (select a.id id, a.name job_name, b.id parent_id, " +
						"case " +
							"when d.name is null AND c.name is null AND b.name is null then concat(a.name) " +
					        "when d.name is null AND c.name is null AND b.name is not null then concat(b.name, '/', a.name) " +
					        "when d.name is null AND c.name is not null AND b.name is not null then concat(c.name, '/', b.name, '/', a.name) " +
					        "when d.name is not null AND c.name is not null AND b.name is not null then concat(d.name, '/', c.name, '/', b.name, '/', a.name) " +
						"end order_code " +
					 "from organization a " + 
					 "left join organization b on b.id = a.parent " +
					 "left join organization c on c.id = b.parent " +
					 "left join organization d on d.id = c.parent " +
					 "where a.is_active = 1 " + 
					 ") org " + 
					 "order by org.order_code";
		 
		Query query =  entityManager.createNativeQuery(sql);
        List<Object []> listObj = query.getResultList();
        
        for (Object[] obj : listObj){
        	BaseTree orgTree = new BaseTree();
        	
        	if(obj[0] != null){
        		orgTree.setId(String.valueOf(obj[0]));
        	}
        	if(obj[1] != null){
        		orgTree.setName(String.valueOf(obj[1]));
        	}
        	if(obj[2] != null){
        		orgTree.setParentId(String.valueOf(obj[2]));
        	}
//        	
        	result.add(orgTree);
        }

        return result;
	}

}
