package id.ac.tazkia.hr.repository.data;

import java.util.List;

public interface BaseDataDao<T> {
	List<T> loadDataTree();
}
