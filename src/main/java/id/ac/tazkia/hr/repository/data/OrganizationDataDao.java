package id.ac.tazkia.hr.repository.data;

import id.ac.tazkia.hr.dto.BaseTree;

public interface OrganizationDataDao extends BaseDataDao<BaseTree> {

}
