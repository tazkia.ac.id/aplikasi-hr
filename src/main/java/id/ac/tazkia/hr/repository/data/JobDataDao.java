package id.ac.tazkia.hr.repository.data;

import id.ac.tazkia.hr.dto.BaseTree;

public interface JobDataDao extends BaseDataDao<BaseTree> {

}
