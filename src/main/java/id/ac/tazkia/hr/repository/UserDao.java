package id.ac.tazkia.hr.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.ac.tazkia.hr.entity.User;

public interface UserDao extends JpaRepository<User, String> {
	User findByEmail(String email);
}
