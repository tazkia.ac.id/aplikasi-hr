package id.ac.tazkia.hr.repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import id.ac.tazkia.hr.entity.Organization;

public interface OrganizationDao extends JpaRepository<Organization, UUID>{
	Optional<Organization> findById(UUID id);
	List<Organization> findAll();
}
