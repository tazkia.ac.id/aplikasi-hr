package id.ac.tazkia.hr.repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import id.ac.tazkia.hr.entity.Company;

public interface CompanyDao extends JpaRepository<Company, UUID> {
	Optional<Company> findById(UUID id);
	Company findTopByCompanyTypeOrderByIdDesc(Integer companyType);
	List<Company> findAll();
}
