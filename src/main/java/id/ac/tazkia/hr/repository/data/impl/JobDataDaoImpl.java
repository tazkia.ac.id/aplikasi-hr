package id.ac.tazkia.hr.repository.data.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import id.ac.tazkia.hr.dto.BaseTree;
import id.ac.tazkia.hr.repository.data.JobDataDao;

@Repository
public class JobDataDaoImpl implements JobDataDao {
	
	@PersistenceContext
    private EntityManager entityManager;
	
	@Override
	public List<BaseTree> loadDataTree() {
		List<BaseTree> result = new ArrayList<>();
		String	sql = "select BIN_TO_UUID(j.id), j.job_name, BIN_TO_UUID(j.parent_id) " +
					  "from (select a.id id, a.name job_name, b.id parent_id, " +
							"case " +
						        "when d.name is null AND c.name is null AND b.name is null then concat(a.name) " +
						        "when d.name is null AND c.name is null AND b.name is not null then concat(b.name, '/', a.name) " +
						        "when d.name is null AND c.name is not null AND b.name is not null then concat(c.name, '/', b.name, '/', a.name) " +
						        "when d.name is not null AND c.name is not null AND b.name is not null then concat(d.name, '/', c.name, '/', b.name, '/', a.name) " +
							"end order_code " +
							"from job a " +
							"left join job b on b.id = a.job_parent " +
							"left join job c on c.id = b.job_parent " +
							"left join job d on d.id = c.job_parent " +
							"where a.is_active = 1 " +
						") j " +
						"order by j.order_code";
		 
		Query query =  entityManager.createNativeQuery(sql);
        List<Object []> listObj = query.getResultList();
        
        for (Object[] obj : listObj){
        	BaseTree jobTree = new BaseTree();
        	
        	if(obj[0] != null){
        		jobTree.setId(String.valueOf(obj[0]));
        	}
        	if(obj[1] != null){
        		jobTree.setName(String.valueOf(obj[1]));
        	}
        	if(obj[2] != null){
        		jobTree.setParentId(String.valueOf(obj[2]));
        	}

        	result.add(jobTree);
        }

        return result;
	}

}
