package id.ac.tazkia.hr.service;

import id.ac.tazkia.hr.entity.User;

public interface UserService {
	User findUserByEmail(String email);
	User findUserByEmailAndPassword(String email, String password);
	User registerUser(User user);
}
