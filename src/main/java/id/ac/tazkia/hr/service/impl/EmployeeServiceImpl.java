package id.ac.tazkia.hr.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.ac.tazkia.hr.entity.Employee;
import id.ac.tazkia.hr.repository.EmployeeDao;
import id.ac.tazkia.hr.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService {
	@Autowired
	private EmployeeDao empDao;
	
	@Override
	public Optional<Employee> findById(String id) {
		return empDao.findById(id);
	}

	@Override
	public List<Employee> findAll() {
		return empDao.findAll();
	}

	@Override
	public void saveOrUpdate(Employee employee) {
		empDao.save(employee);
	}

}
