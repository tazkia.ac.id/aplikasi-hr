package id.ac.tazkia.hr.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.ac.tazkia.hr.entity.JobLevel;
import id.ac.tazkia.hr.repository.JobLevelDao;
import id.ac.tazkia.hr.service.JobLevelService;

@Service
public class JobLevelServiceImpl implements JobLevelService {
	@Autowired
	private JobLevelDao jobLevelDao;
		
	@Override
	public Optional<JobLevel> findById(UUID id) {
		return jobLevelDao.findById(id);
	}

	@Override
	public List<JobLevel> findAll() {
		return jobLevelDao.findAll();
	}

	@Override
	public void saveOrUpdate(JobLevel jobLevel) {
		jobLevelDao.save(jobLevel);
	}
	
}
