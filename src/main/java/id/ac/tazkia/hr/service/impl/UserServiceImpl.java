package id.ac.tazkia.hr.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import id.ac.tazkia.hr.entity.User;
import id.ac.tazkia.hr.repository.UserDao;
import id.ac.tazkia.hr.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserDao userDao;
	
	@Autowired
    private PasswordEncoder passwordEncoder;
		
	@Override
	public User findUserByEmail(String email) {
		return userDao.findByEmail(email);
	}
	
	@Override
	public User findUserByEmailAndPassword(String email, String password) {
		User user = userDao.findByEmail(email);
		if(user != null){
			if(passwordEncoder.matches(password, user.getPassword())){
				return user;
			}
		}
		return null;
	}
	
	@Override
	public User registerUser(User user) {
		User existUser = userDao.findByEmail(user.getEmail());
		if(existUser != null){
			existUser.setPassword(passwordEncoder.encode(user.getPassword()));
			userDao.save(existUser);
			return existUser;
		} else {
			return null;
		}
	}

}
