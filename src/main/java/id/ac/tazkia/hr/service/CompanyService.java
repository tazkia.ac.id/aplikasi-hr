package id.ac.tazkia.hr.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import id.ac.tazkia.hr.entity.Company;

public interface CompanyService {
	void saveOrUpdate(Company company);
	Optional<Company> findById(UUID id);
	Company findTopByCompanyTypeOrderByIdDesc(Integer companyType);
	List<Company> findAll();
}
