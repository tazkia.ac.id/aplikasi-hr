package id.ac.tazkia.hr.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.ac.tazkia.hr.dto.BaseTree;
import id.ac.tazkia.hr.entity.Organization;
import id.ac.tazkia.hr.repository.OrganizationDao;
import id.ac.tazkia.hr.repository.data.OrganizationDataDao;
import id.ac.tazkia.hr.service.OrganizationService;

@Service
public class OrganizationServiceImpl implements OrganizationService {
	@Autowired
	private OrganizationDao orgDao;
	
	@Autowired
	private OrganizationDataDao orgDataDao;

	@Override
	public Optional<Organization> findById(UUID id) {
		return orgDao.findById(id);
	}

	@Override
	public List<Organization> findAll() {
		return orgDao.findAll();
	}
	
	@Override
	public List<BaseTree> loadOrganizationTree() {
		return orgDataDao.loadDataTree();
	}

	@Override
	public void saveOrUpdate(Organization organization) {
		orgDao.save(organization);
	}
	
}
