package id.ac.tazkia.hr.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.ac.tazkia.hr.dto.BaseTree;
import id.ac.tazkia.hr.entity.Job;
import id.ac.tazkia.hr.repository.JobDao;
import id.ac.tazkia.hr.repository.data.JobDataDao;
import id.ac.tazkia.hr.service.JobService;

@Service
public class JobServiceImpl implements JobService {
	@Autowired
	private JobDao jobDao;
	
	@Autowired
	private JobDataDao jobDataDao;
	
	@Override
	public Optional<Job> findById(UUID id) {
		return jobDao.findById(id);
	}

	@Override
	public List<Job> findAll() {
		return jobDao.findAll();
	}
	
	@Override
	public void saveOrUpdate(Job job) {
		jobDao.save(job);
	}

	@Override
	public List<BaseTree> loadJobPositionTree() {
		return jobDataDao.loadDataTree();
	}

}
