package id.ac.tazkia.hr.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.ac.tazkia.hr.entity.Company;
import id.ac.tazkia.hr.repository.CompanyDao;
import id.ac.tazkia.hr.service.CompanyService;

@Service
public class CompanyServiceImpl implements CompanyService {
	@Autowired
	private CompanyDao companyDao;

	@Override
	public void saveOrUpdate(Company company) {
		companyDao.save(company);
	}
	
	@Override
	public Optional<Company> findById(UUID id) {
		return companyDao.findById(id);
	}
	
	@Override
	public Company findTopByCompanyTypeOrderByIdDesc(Integer companyType){
		return companyDao.findTopByCompanyTypeOrderByIdDesc(companyType);
	}

	@Override
	public List<Company> findAll() {
		return companyDao.findAll();
	}
	
}
