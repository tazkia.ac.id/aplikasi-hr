package id.ac.tazkia.hr.service;

import java.util.List;
import java.util.Optional;

import id.ac.tazkia.hr.entity.Employee;

public interface EmployeeService {
	Optional<Employee> findById(String id);
	List<Employee> findAll();
	void saveOrUpdate(Employee employee);
}
