package id.ac.tazkia.hr.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import id.ac.tazkia.hr.dto.BaseTree;
import id.ac.tazkia.hr.entity.Job;

public interface JobService {
	Optional<Job> findById(UUID id);
	List<Job> findAll();
	void saveOrUpdate(Job job);
	List<BaseTree> loadJobPositionTree();
}
