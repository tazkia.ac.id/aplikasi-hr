package id.ac.tazkia.hr.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserRequest;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserService;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Service;

import id.ac.tazkia.hr.dto.UserInfo;
import id.ac.tazkia.hr.entity.User;
import id.ac.tazkia.hr.repository.UserDao;

@Service
public class CustomUserService extends OidcUserService {
	@Autowired
	private UserDao userDao;
	
	@Override
	public OidcUser loadUser(OidcUserRequest userRequest) throws OAuth2AuthenticationException {
		OidcUser oidcUser = super.loadUser(userRequest);
		Map attributes = oidcUser.getAttributes();
		UserInfo userInfo = new UserInfo();
		userInfo.setEmail((String) attributes.get("email"));
		userInfo.setImageUrl((String) attributes.get("picture"));
		updateUser(userInfo);
		
		return oidcUser;
	}
	
	private void updateUser(UserInfo userInfo) {
		User user = userDao.findByEmail(userInfo.getEmail());
		if(user != null){
			user.setEmail(userInfo.getEmail());
			if(user.getImageUrl() == null){
				user.setImageUrl(userInfo.getImageUrl());
			}
			userDao.save(user);
		}
	} 
}
