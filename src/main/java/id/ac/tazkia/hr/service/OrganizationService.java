package id.ac.tazkia.hr.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import id.ac.tazkia.hr.dto.BaseTree;
import id.ac.tazkia.hr.entity.Organization;

public interface OrganizationService {
	Optional<Organization> findById(UUID id);
	List<Organization> findAll();
	List<BaseTree> loadOrganizationTree();
	void saveOrUpdate(Organization organization);
}
