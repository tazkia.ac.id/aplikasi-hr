package id.ac.tazkia.hr.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import id.ac.tazkia.hr.entity.JobLevel;

public interface JobLevelService {
	Optional<JobLevel> findById(UUID id);
	List<JobLevel> findAll();
	void saveOrUpdate(JobLevel jobLevel);
}
