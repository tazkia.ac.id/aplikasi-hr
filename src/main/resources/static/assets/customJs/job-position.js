(function($) {
    'use strict';
    function notif(message) {
        resetToastPosition();
        $.toast({
            text: message,
            showHideTransition: 'slide',
            icon: 'error',
            loaderBg: '#f2a654',
            position: 'top-right'
          })
    }

    $(document).ready(function() {
    	loadJobPositionTreeData();
    	
    	$('#parentIdJobPosition').select2({
    		placeholder: 'Select Parent',
    		tags: true,
		    multiple: false,
		    tokenSeparators: [',', ' '],
		    minimumInputLength: 2,
		    minimumResultsForSearch: 10,
		    ajax: {
		        url: 'http://localhost:8080/api/job-all',
		        dataType: 'json',
		        type: 'GET',
		        data: function (params) {
		            var queryParameters = {
		                term: params.term
		            }
		            return queryParameters;
		        },
		        processResults: function (data) {
		            return {
		                results: $.map(data, function (item) {
		                    return {
		                        text: item.name,
		                        id: item.id
		                    }
		                })
		            };
		        }
		    }
    	});
    	
    	$('#parentIdEdit').select2({
    		placeholder: 'Select Parent',
    		tags: true,
		    multiple: false,
		    tokenSeparators: [',', ' '],
		    minimumInputLength: 2,
		    minimumResultsForSearch: 10,
		    ajax: {
		        url: 'http://localhost:8080/api/job-all',
		        dataType: 'json',
		        type: 'GET',
		        data: function (params) {
		            var queryParameters = {
		                term: params.term
		            }
		            return queryParameters;
		        },
		        processResults: function (data) {
		            return {
		                results: $.map(data, function (item) {
		                    return {
		                        text: item.name,
		                        id: item.id
		                    }
		                })
		            };
		        }
		    }
    	});
    	
        $('#jobPosition').validate({
            rules: {
                name: {
                    required: true,
                    maxlength: 100
                },
            },
            message : {},
            errorPlacement: function(label, element) {
                var idE = element.attr('id');
                var errorSelector = '.validation_error_message[for="' + idE + '"]';
                var $element = $(errorSelector);
                if ($element.length) {
                    $(errorSelector).html(label.html());
                } else {
                    label.addClass('mt-2 text-danger');
                    label.insertAfter(element);
                }
            },
            highlight: function(element, errorClass) {
                $(element).parent().addClass('has-danger')
                $(element).addClass('form-control-danger')
            }
        })

        $('#saveJobPositionPrimary').click(function() {
            if ($('#jobPositionName').val() == "") {
                notif("Job position name must be filled")
            }
            
            if ($('#jobPositionName').val().length > 100) {
                notif("Please enter job position name no more than 100 characters")
            }
        });
       
    });
    
    function loadJobPositionTreeData(){
    	var root = "http://localhost:8080/api/job-tree";
		$.ajax({
		 	type: 'GET',
		 	url: root,
		 	data:{},
		 	success: function(data) {
		 		var jobTree = JSON.parse(data);
		 		var elmnt = "";
		 		var beforeJobName = "";
		 		
		 		for(var i = 0; i < jobTree.length; i++){
		 			var job = jobTree[i]; 
		 			if(job.parentId == null){
    	 				elmnt = "<li id='elm_"+ job.id +"'><div>" + job.name + " <a href=\"#\" title=\"edit\" data-toggle=\"modal\" data-target=\"#exampleModal-2\"" + 
        	 			" data-whatever=\"@mdo\" data-id='" + job.id + "'><span class=\"fa fa-pencil-square-o\"></span></a> <a href=\"#\" title=\"delete\" onclick=\"showSwal('warning-delete-or-cancel', '" + job.id + "')\">" +
        	 			" <span class=\"fa fa-trash-o\"></span></a></div></li>";
    	 				
    	 				$(elmnt).appendTo("#jobTree");
    	 			} else {
    	 				if(job.name === beforeJobName){
        	 				continue;
        	 			}
    	 				elmnt = "<ul><li id='elm_"+ job.id +"'><div>" + job.name + " <a href=\"#\" title=\"edit\" data-toggle=\"modal\" data-target=\"#exampleModal-2\"" + 
        	 			" data-whatever=\"@mdo\" data-id='" + job.id + "'><span class=\"fa fa-pencil-square-o\"></span></a> <a href=\"#\" title=\"delete\" onclick=\"showSwal('warning-delete-or-cancel', '" + job.id + "')\">" +
        	 			" <span class=\"fa fa-trash-o\"></span></a></div></li></ul>";
    	 				
    	 				$(elmnt).appendTo("#elm_" + job.parentId);
    	 			}
		 			
		 			beforeJobName = job.name;
		 		}
		 	}
		});
   };
   
   // modal edit
   $('#exampleModal-2').on('show.bs.modal', function (e) {
   	var button = $(e.relatedTarget);
   	var idEdit = button.data('id');
   	var root = 'http://localhost:8080/api/job-detail?id=' + idEdit;
	   	$.ajax({
	   	 	type: 'GET',
	   	 	url: root,
	   	 	data:{},
	   	 	success: function(data) {
	   	 		var job = JSON.parse(data);
	   	 		$('#idEdit').val(job.id);
	   	 		$('#jobNameEdit').val(job.name);
	   	 		$('#oldParentId').val(job.parent);
	   	 		setOldParentName(job.parent);
	   	 	}
	   	});
	});
   
   function setOldParentName(jobId){
	   var root = 'http://localhost:8080/api/job-detail?id=' + jobId;
	   $.ajax({
		   type: 'GET',
		   url: root,
		   data:{},
		   success: function(data) {
			   var job = JSON.parse(data);	
			   $('#oldParentName').val(job.name);
			   var $parentIdEdit = $("<option selected></option>").val($("#oldParentId").val()).text($("#oldParentName").val());
			   $("#parentIdEdit").append($parentIdEdit).trigger('change');
		   }
	   });
   };
   // end modal edit
   
   showSwal = function(type, idDeleted) {
	   	if (type === 'warning-delete-or-cancel') {
		      swal({
		        title: 'Are you sure?',
		        text: "You won't be able to revert this!",
		        icon: 'warning',
		        showCancelButton: true,
		        confirmButtonColor: '#3f51b5',
		        cancelButtonColor: '#ff4081',
		        confirmButtonText: 'Great ',
		        buttons: {
		          cancel: {
		            text: "Cancel",
		            value: null,
		            visible: true,
		            className: "btn btn-danger",
		            closeModal: true,
		          },
		          confirm: {
		            text: "OK",
		            value: true,
		            visible: true,
		            className: "btn btn-primary",
		            closeModal: true
		          }
		        }
		      }).then(function(value) {
				  	if (value) {
				  		deleteJobById(idDeleted);
				  	}
		        });
	   	}
   }
   
   function deleteJobById(idDeleted){
	   var root = 'http://localhost:8080/api/job-delete?id=' + idDeleted;
	   var redirectUrl = 'http://localhost:8080/job-position';
	   $.ajax({
		   type: 'GET',
   			url: root,
   			data:{},
   			success: function(data) {
   				window.location.href = redirectUrl;
   			}
	   });
   }
      
})(jQuery);