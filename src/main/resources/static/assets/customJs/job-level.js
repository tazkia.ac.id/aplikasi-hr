(function($) {
    'use strict';
    function notif(message) {
        resetToastPosition();
        $.toast({
            text: message,
            showHideTransition: 'slide',
            icon: 'error',
            loaderBg: '#f2a654',
            position: 'top-right'
          })
    }
    $(document).ready(function() {
    	loadJobLevelTreeData();
    	
        $("#saveJobLevelPrimary").submit(function(e) {
            e.preventDefault();
        });

        $("#jobLevelFormPrimary").validate({
            rules: {
                name: {
                    required: true,
                    maxlength: 50,
                }
            },
            message: { },
            errorPlacement: function(label, element) {
                var idE = element.attr('id');
                var errorSelector = '.validation_error_message[for="' + idE + '"]';
                var $element = $(errorSelector);
                if ($element.length) {
                    $(errorSelector).html(label.html());
                } else {
                    label.addClass('mt-2 text-danger');
                    label.insertAfter(element);
                }
            },
            highlight: function(element, errorClass) {
                $(element).parent().addClass('has-danger')
                $(element).addClass('form-control-danger')
            }
        });
        $("#saveJobLevelPrimary").click(function() {
            if ($('#jobLevelName').val() == "") {
                notif("Job Level name must be filled")
            }
            
            if ($('#jobLevelName').val().length > 50) {
                notif("Please enter job level name no more than 50 characters")
            }
        });
    });
    
    function loadJobLevelTreeData(){
    	var root = "http://localhost:8080/api/job-level-all";
		$.ajax({
		 	type: 'GET',
		 	url: root,
		 	data:{},
		 	success: function(data) {
		 		var jobLevelTree = JSON.parse(data);
		 		
		 		for(var i = 0; i < jobLevelTree.length; i++){
		 			var job = jobLevelTree[i]; 
		 			
		 			var elmnt = "<li id='elm_" + job.id + "'><div>" + job.name + "<a href=\"#\" title=\"delete\" onclick=\"showSwal('warning-delete-or-cancel', '" + job.id + "')\">" +
	   	 			" <span class=\"fa fa-trash-o\"></span></a></div></li>";
	 				
	 				$(elmnt).appendTo("#jobLevelTree");
		 		}
		 	}
		});
   };
   
   showSwal = function(type, idDeleted) {
	   	if (type === 'warning-delete-or-cancel') {
		      swal({
		        title: 'Are you sure?',
		        text: "You won't be able to revert this!",
		        icon: 'warning',
		        showCancelButton: true,
		        confirmButtonColor: '#3f51b5',
		        cancelButtonColor: '#ff4081',
		        confirmButtonText: 'Great ',
		        buttons: {
		          cancel: {
		            text: "Cancel",
		            value: null,
		            visible: true,
		            className: "btn btn-danger",
		            closeModal: true,
		          },
		          confirm: {
		            text: "OK",
		            value: true,
		            visible: true,
		            className: "btn btn-primary",
		            closeModal: true
		          }
		        }
		      }).then(function(value) {
				  	if (value) {
				  		deleteJobById(idDeleted);
				  	}
		        });
	   	}
	   }
	   
	   function deleteJobById(idDeleted){
		   var root = 'http://localhost:8080/api/job-level-delete?id=' + idDeleted;
		   var redirectUrl = 'http://localhost:8080/job-level';
		   $.ajax({
			   type: 'GET',
	   			url: root,
	   			data:{},
	   			success: function(data) {
	   				window.location.href = redirectUrl;
	   			}
		   });
	   }
   
})(jQuery);