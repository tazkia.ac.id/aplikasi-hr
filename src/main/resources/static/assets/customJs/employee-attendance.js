(function($){
    $('.datepicker').datepicker({
        format : 'dd-mm-yyyy',
    });

    $(function() {
        $('table.display').DataTable({
            "language": {
                search: ""
            },
            "scrollX": true,
            "scrollCollapse": true,
            // "fixedColumns":   {
            //     leftColumns: 1
            // }
        })
        $('table.display').each(function() {
            var datatable = $(this);
            // SEARCH - Add the placeholder for Search and Turn this into in-line form control
            var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
            search_input.attr('placeholder', 'Search');
            search_input.removeClass('form-control-sm');
            // LENGTH - Inline-Form control
            var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
            length_sel.removeClass('form-control-sm');
        });
    });

    $('#optionsRadios1').change(function() {
        if(this.checked) {
            document.getElementById('effectiveDateShift').disabled = false;
            document.getElementById('currentShift').value = "";
            document.getElementById('currentShift').disabled = false;
            document.getElementById('newShift').disabled = false;

            document.getElementById('effectiveDateAttendance').disabled = true;
            document.getElementById('ShiftAttendance').value = "";
            document.getElementById('ShiftAttendance').disabled = true;
            document.getElementById('chkIn').checked = false;
            document.getElementById('chkOut').checked = false;
            document.getElementById('chkIn').disabled = true;
            document.getElementById('chkOut').disabled = true;
            document.getElementById('checkIn').disabled = true;
            document.getElementById('checkOut').disabled = true;
            document.getElementById('checkIn').value = "";
            document.getElementById('checkOut').value = "";
        }
    });

    $('#optionsRadios2').change(function() {
        if(this.checked) {
            document.getElementById('effectiveDateShift').disabled = true;
            document.getElementById('currentShift').value = "";
            document.getElementById('newShift').disabled = true;

            document.getElementById('effectiveDateAttendance').disabled = false;
            document.getElementById('ShiftAttendance').value = "";
            document.getElementById('chkIn').checked = true;
            document.getElementById('chkOut').checked = true;
            document.getElementById('chkIn').disabled = false;
            document.getElementById('chkOut').disabled = false;
            document.getElementById('checkIn').disabled = false;
            document.getElementById('checkOut').disabled = false;
            document.getElementById('checkIn').value = "";
            document.getElementById('checkOut').value = "";
        }
    });

})(jQuery);