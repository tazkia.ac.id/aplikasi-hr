// $(document).ready(function() {
//     $("#order-listing").Datatable({
//         "scrollX": true,
//     });
// });

$(document).ready(function() {
    // var table = $('#order-listing').DataTable({
    //     scrollY:        "300px",
    //     scrollX:        true,
    //     scrollCollapse: true,
    //     paging:         false,
    //     fixedColumns:   {
    //         leftColumns: 2
    //     }
    // });

    $('#order-listing').DataTable({
        "aLengthMenu": [
            [5, 10, 15, -1],
            [5, 10, 15, "All"]
        ],
        "iDisplayLength": 10,
        "language": {
            search: ""
        },
        "scrollX": true,
        "scrollCollapse": true,
        "fixedColumns":   {
            leftColumns: 3
        }
    });
    
    $('#order-listing').each(function() {
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.removeClass('form-control-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.removeClass('form-control-sm');
    });
});

