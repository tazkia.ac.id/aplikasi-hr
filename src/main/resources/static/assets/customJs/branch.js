(function($) {
    'use strict';
    function notif(message) {
        resetToastPosition();
        $.toast({
            text: message,
            showHideTransition: 'slide',
            icon: 'error',
            loaderBg: '#f2a654',
            position: 'top-right'
          })
    }
    
    $(function() {
    	loadLogoImage();
    	loadSignatureImage();
    	
        // hide and show npwp same with parent branch
        $('.parent').toggle();
        $('#npwpSame').click(function() {
            var inputValue = $(this).attr("value");
            $('.'+ inputValue).toggle();
            loadParentBranch();
        });
        
        $("#parentSameNpwp").change(function() {
            if ($("#parentSameNpwp").val().length != 0) {
            	var parentId = $("#parentSameNpwp").val();
            	loadParentBranchById(parentId);
            }
        });
        
        $('.branchFormValidation').validate({
            rules: {
                name: {
                    required: true,
                    maxlength: 50
                },
                address: {
                    required: true,
                    maxlength: 255
                },
                postalCode: {
                    required: true,
                    maxlength: 6
                },
                umr: {
                    required: true,
                    maxlength: 15
                },
                phone: {
                    required: true,
                    maxlength: 15
                },
                fax: {
                    required: true,
                    maxlength: 50
                },
                taxPerson: {
                    required: false,
                    maxlength: 50
                },
                taxPersonNpwp: {
                    required: false,
                    maxlength: 30
                },
                taxName: {
                    required: false,
                    maxlength: 50
                },
                npwp: {
                    required: false,
                    maxlength: 30
                },
            },
            message : {},
            errorPlacement: function(label, element) {
                var idE = element.attr('id');
                var errorSelector = '.validation_error_message[for="' + idE + '"]';
                var $element = $(errorSelector);
                if ($element.length) {
                    $(errorSelector).html(label.html());
                } else {
                    label.addClass('mt-2 text-danger');
                    label.insertAfter(element);
                }
            },
            highlight: function(element, errorClass) {
                $(element).parent().addClass('has-danger')
                $(element).addClass('form-control-danger')
            }
        })

        $('#save').click(function() {
            if ($('#branchName').val() == "") {
                notif("Branch name must be filled")
            } else if ($('#addressBranch').val() == "") {
                notif("Address branch must be filled")
            } else if ($('#postalCode').val() == "") {
                notif("Postal code must be filled")
            } else if ($('#umr').val() == "") {
                notif("UMR must be filled")
            } else if ($('#branchPhone').val() == "") {
                notif("Branch phone number must be filled")
//            } else if ($('#taxPersonName').val() == "") {	//not mandatory
//                notif("Tax person name must be filled")
//            } else if ($('#taxPersonNPWP').val() == "") {
//                notif("Tax person NPWP must be filled")
//            } else if ($('#branchTaxName').val() == "") {
//                notif("Branch tax name must be filled")
//            } else if ($('#branchNPWP').val() == "") {
//                notif("Branch NPWP must be filled")
            }

            if ($('#branchName').val().length > 50) {
                notif("Please enter branch name no more than 50 characters")
            } else if ($('#addressBranch').val().length > 255) {
                notif("Please enter Address no more than 50 characters")
            } else if ($('#postalCode').val().length > 6) {
                notif("Please enter postal code no more than 6 characters")
            } else if ($('#umr').val().length > 15) {
                notif("Please enter umr no more than 15 characters")
            } else if ($('#branchPhone').val().length > 15) {
                notif("Please enter branch phone no more than 15 characters")
	        } else if ($('#branchFax').val().length > 50) {
	            notif("Please enter branch fax no more than 50 characters")
	        } else if ($('#taxPersonName').val().length > 50) {
                notif("Please enter tax person name no more than 50 characters")
            } else if ($('#taxPersonNPWP').val().length > 30) {
                notif("Please enter tax person npwp no more than 30 characters")
            } else if ($('#branchTaxName').val().length > 50) {
                notif("Please enter branch tax name no more than 50 characters")
            } else if ($('#branchNPWP').val().length > 30) {
                notif("Please enter branch npwp no more than 30 characters")
            }
        });
       
    });
        
    // $('.datepicker').datepicker({
	// 	format : 'dd MM, yyyy',
    // });
    
    function loadParentBranch(){
    	$('#parentSameNpwp').select2({
    		placeholder: 'Select Parent',
    		tags: true,
    	    multiple: false,
    	    tokenSeparators: [',', ' '],
    	    minimumInputLength: 2,
    	    minimumResultsForSearch: 10,
    	    ajax: {
    	        url: 'http://localhost:8080/api/company-all',
    	        dataType: 'json',
    	        type: 'GET',
    	        data: function (params) {
    	            var queryParameters = {
    	                term: params.term
    	            }
    	            return queryParameters;
    	        },
    	        processResults: function (data) {
    	            return {
    	                results: $.map(data, function (item) {
    	                    return {
    	                        text: item.name,
    	                        id: item.id
    	                    }
    	                })
    	            };
    	        }
    	    }
    	});
    }
        
    function loadParentBranchById(parentId){
	   	 $.ajax({
	   	 	type: 'GET',
	   	 	url: 'http://localhost:8080/api/parent-branch-npwp?parent=' + parentId,
	   	 	data:{},
	   	 	success: function(data) {
	   	 		var company = JSON.parse(data);
		   	 	$('#taxPersonName').val(company.taxPerson),
		    	$('#taxPersonNPWP').val(company.taxPersonNpwp),
		    	$('#branchTaxName').val(company.taxName),
		    	$('#branchNPWP').val(company.npwp)
	   	 	}
	   	 });
    }
    
    function loadLogoImage(){
    	var image = $('#logoImg').val();
    	if(image){
    		var imagePath = '../images/' + image;
        	var drEvent = $('#logo').dropify({
        	  defaultFile: imagePath
        	});
        	drEvent = drEvent.data('dropify');
        	drEvent.resetPreview();
        	drEvent.clearElement();
        	drEvent.settings.defaultFile = imagePath;
        	drEvent.destroy();
        	drEvent.init();
    	}
    }
    
    function loadSignatureImage(){
    	var image = $('#signatureImg').val();
    	if(image){
    		var imagePath = '../images/' + image;
        	var drEvent = $('#signature').dropify({
        	  defaultFile: imagePath
        	});
        	drEvent = drEvent.data('dropify');
        	drEvent.resetPreview();
        	drEvent.clearElement();
        	drEvent.settings.defaultFile = imagePath;
        	drEvent.destroy();
        	drEvent.init();
    	}
    }
   
})(jQuery);