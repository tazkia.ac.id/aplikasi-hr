(function($){
    $('.datepicker').datepicker({
        format : 'dd-mm-yyyy',
    });

    $(function() {
        $('table.display').DataTable({
            "language": {
                search: ""
            },
            "scrollX": true,
            "scrollCollapse": true,
        })
        $('table.display').each(function() {
            var datatable = $(this);
            // SEARCH - Add the placeholder for Search and Turn this into in-line form control
            var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
            search_input.attr('placeholder', 'Search');
            search_input.removeClass('form-control-sm');
            // LENGTH - Inline-Form control
            var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
            length_sel.removeClass('form-control-sm');
        });
    });
    
    $('a[data-toggle="pill"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
           .columns.adjust();
    });

    $('.delegateCheck').change(function() {
        if(this.checked) {
            $(".delegateShow").css("display", "block");
        } else {
            $(".delegateShow").css("display", "none");
        }
    });
})(jQuery);