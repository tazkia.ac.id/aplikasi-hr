(function($) {
    'use strict';
    function notif(message) {
        resetToastPosition();
        $.toast({
            text: message,
            showHideTransition: 'slide',
            icon: 'error',
            loaderBg: '#f2a654',
            position: 'top-right'
          })
    }

    $(function() {
    	loadOrganizationData();
    	
    	$('#orgParentPrimary').select2({
    		placeholder: 'Select Parent',
    		tags: true,
		    multiple: false,
		    tokenSeparators: [',', ' '],
		    minimumInputLength: 2,
		    minimumResultsForSearch: 10,
		    ajax: {
		        url: 'http://localhost:8080/api/organization-all',
		        dataType: 'json',
		        type: 'GET',
		        data: function (params) {
		            var queryParameters = {
		                term: params.term
		            }
		            return queryParameters;
		        },
		        processResults: function (data) {
		            return {
		                results: $.map(data, function (item) {
		                    return {
		                        text: item.name,
		                        id: item.id
		                    }
		                })
		            };
		        }
		    }
    	});
    	
    	$('#orgParent').select2({
    		placeholder: 'Select Parent',
    		tags: true,
		    multiple: false,
		    tokenSeparators: [',', ' '],
		    minimumInputLength: 2,
		    minimumResultsForSearch: 10,
		    ajax: {
		        url: 'http://localhost:8080/api/organization-all',
		        dataType: 'json',
		        type: 'GET',
		        data: function (params) {
		            var queryParameters = {
		                term: params.term
		            }
		            return queryParameters;
		        },
		        processResults: function (data) {
		            return {
		                results: $.map(data, function (item) {
		                    return {
		                        text: item.name,
		                        id: item.id
		                    }
		                })
		            };
		        }
		    }
    	});
        
        $('#orgFormPrimary').validate({
            rules: {
                name: {
                    required: true,
                    maxlength: 100
                },
            },
            message : {},
            errorPlacement: function(label, element) {
                var idE = element.attr('id');
                var errorSelector = '.validation_error_message[for="' + idE + '"]';
                var $element = $(errorSelector);
                if ($element.length) {
                    $(errorSelector).html(label.html());
                } else {
                    label.addClass('mt-2 text-danger');
                    label.insertAfter(element);
                }
            },
            highlight: function(element, errorClass) {
                $(element).parent().addClass('has-danger')
                $(element).addClass('form-control-danger')
            }
        })

        $('#savePrimary').click(function() {
            if ($('#orgNamePrimary').val() == "") {
                notif("Organization name must be filled")
            }
            
            if ($('#orgNamePrimary').val().length > 100) {
                notif("Please enter organization name no more than 100 characters")
            }
        });

        $('#orgForm').validate({
            rules: {
                name: {
                    required: true,
                    maxlength: 100
                },
            },
            message : {},
            errorPlacement: function(label, element) {
                var idE = element.attr('id');
                var errorSelector = '.validation_error_message[for="' + idE + '"]';
                var $element = $(errorSelector);
                if ($element.length) {
                    $(errorSelector).html(label.html());
                } else {
                    label.addClass('mt-2 text-danger');
                    label.insertAfter(element);
                }
            },
            highlight: function(element, errorClass) {
                $(element).parent().addClass('has-danger')
                $(element).addClass('form-control-danger')
            }
        })

        $('#saveEditBtn').click(function() {
            if ($('#orgName').val() == "") {
                notif("Organization name must be filled")
            } 
            if ($('#orgName').val().length > 100) {
                notif("Please enter organization name no more than 100 characters")
            }
        });
        
    });
    
    function loadOrganizationData(){
    	 var root = "http://localhost:8080/api/organization-tree";
    	 $.ajax({
    	 	type: 'GET',
    	 	url: root,
    	 	data:{},
    	 	success: function(data) {
    	 		var orgTree = JSON.parse(data);
    	 		var elmnt = "";
    	 		var beforeOrgName = "";
    	 		
    	 		for(var i = 0; i < orgTree.length; i++){
    	 			var org = orgTree[i]; 
    	 			if(org.parentId == null){
    	 				elmnt = "<li id='elm_"+ org.id +"'><div>" + org.name + " <a href=\"#\" title=\"edit\" data-toggle=\"modal\" data-target=\"#exampleModal-2\"" + 
        	 			" data-whatever=\"@mdo\" data-id='" + org.id + "'><span class=\"fa fa-pencil-square-o\"></span></a> <a href=\"#\" title=\"delete\" onclick=\"showSwal('warning-delete-or-cancel', '" + org.id + "')\">" +
        	 			" <span class=\"fa fa-trash-o\"></span></a></div></li>";
    	 				
    	 				$(elmnt).appendTo("#orgTree");
    	 			} else {
    	 				if(org.name === beforeOrgName){
        	 				continue;
        	 			}
    	 				elmnt = "<ul><li id='elm_"+ org.id +"'><div>" + org.name + " <a href=\"#\" title=\"edit\" data-toggle=\"modal\" data-target=\"#exampleModal-2\"" + 
        	 			" data-whatever=\"@mdo\" data-id='" + org.id + "'><span class=\"fa fa-pencil-square-o\"></span></a> <a href=\"#\" title=\"delete\" onclick=\"showSwal('warning-delete-or-cancel', '" + org.id + "')\">" +
        	 			" <span class=\"fa fa-trash-o\"></span></a></div></li></ul>";
    	 				
    	 				$(elmnt).appendTo("#elm_" + org.parentId);
    	 			}
    	 				    	 			
    	 			beforeOrgName = org.name;
    	 		}
    	 	}
    	 });
    };
        
    //modal edit organization
    $('#exampleModal-2').on('show.bs.modal', function (e) {
    	var button = $(e.relatedTarget);
    	var idEdit = button.data('id');
    	var root = 'http://localhost:8080/api/organization-detail?id=' + idEdit;
 	   	$.ajax({
 	   	 	type: 'GET',
 	   	 	url: root,
 	   	 	data:{},
 	   	 	success: function(data) {
 	   	 		var org = JSON.parse(data);
 	   	 		$('#idEdit').val(org.id);
 	   	 		$('#orgName').val(org.name);
 	   	 		$('#oldParentId').val(org.parent);
 	   	 		setOldParentName(org.parent);
 	   	 	}
 	   	});
	});
    
    function setOldParentName(orgId){
    	var root = 'http://localhost:8080/api/organization-detail?id=' + orgId;
 	   	$.ajax({
 	   	 	type: 'GET',
 	   	 	url: root,
 	   	 	data:{},
 	   	 	success: function(data) {
 	   	 		var org = JSON.parse(data);	
 	   	 		$('#oldParentName').val(org.name);
	 	   	 	var $parentIdEdit = $("<option selected></option>").val($("#oldParentId").val()).text($("#oldParentName").val());
	 	   	 	$("#orgParent").append($parentIdEdit).trigger('change');
 	   	 	}
 	   	});
    };
    // end modal edit
    
    showSwal = function(type, idDeleted) {
    	if (type === 'warning-delete-or-cancel') {
	      swal({
	        title: 'Are you sure?',
	        text: "You won't be able to revert this!",
	        icon: 'warning',
	        showCancelButton: true,
	        confirmButtonColor: '#3f51b5',
	        cancelButtonColor: '#ff4081',
	        confirmButtonText: 'Great ',
	        buttons: {
	          cancel: {
	            text: "Cancel",
	            value: null,
	            visible: true,
	            className: "btn btn-danger",
	            closeModal: true,
	          },
	          confirm: {
	            text: "OK",
	            value: true,
	            visible: true,
	            className: "btn btn-primary",
	            closeModal: true
	          }
	        }
	      }).then(function(value) {
			  	if (value) {
			  		deleteOrganizationById(idDeleted);
			  	}
	        });
    	}
    }
    
    function deleteOrganizationById(idDeleted){
    	var root = 'http://localhost:8080/api/organization-delete?id=' + idDeleted;
    	var redirectUrl = 'http://localhost:8080/organization-structure';
    	$.ajax({
    		type: 'GET',
    		url: root,
    		data:{},
    		success: function(data) {
    			window.location.href = redirectUrl;
    		}
    	});
    }
   
})(jQuery);