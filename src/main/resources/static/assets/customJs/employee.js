(function($) {
	'use strict';
	function notif(message) {
        resetToastPosition();
        $.toast({
            text: message,
            showHideTransition: 'slide',
            icon: 'error',
            loaderBg: '#f2a654',
            position: 'top-right'
          })
    }
    
    $(function() {
		var form = $("#employee-add-form").show();
		form.children("div").steps({
            headerTag: "h3",
            bodyTag: "section",
			transitionEffect: "slideLeft",
			onStepChanging: function(event, currentIndex, newIndex) {
				if ($('#employeeId').val() == "") {
					notif("Employee Id must be filled")
					return false;
				} else {
					return true;
				};
			},
			onFinishing: function(event, currentIndex) {
				// form.validate().setting.ignore = ":disabled";
				return form.valid();
			},
            onFinished: function(event, currentIndex) {
                form.submit(); 
                //alert("Submitted!");
            }
        }).validate({
			rules: {
				employeeId: { 
					required: true,
                    maxlength: 15,
				}
			},
			messages:{},
            errorPlacement: function(label, element) {
                var idE = element.attr('id');
                var errorSelector = '.validation_error_message[for="' + idE + '"]';
                var $element = $(errorSelector);
                if ($element.length) {
                    $(errorSelector).html(label.html());
                } else {
                    label.addClass('mt-2 text-danger');
                    label.insertAfter(element);
                }
            },
            highlight: function(element, errorClass) {
                $(element).parent().addClass('has-danger')
                $(element).addClass('form-control-danger')
            }
		});
    	
    	$('#companyId').select2({
    		placeholder: 'Select Branch',
    		tags: true,
		    multiple: false,
		    tokenSeparators: [',', ' '],
		    minimumInputLength: 2,
		    minimumResultsForSearch: 10,
		    ajax: {
		        url: 'http://localhost:8080/api/company-all',
		        dataType: 'json',
		        type: 'GET',
		        data: function (params) {
		            var queryParameters = {
		                term: params.term
		            }
		            return queryParameters;
		        },
		        processResults: function (data) {
		            return {
		                results: $.map(data, function (item) {
		                    return {
		                        text: item.name,
		                        id: item.id
		                    }
		                })
		            };
		        }
		    }
    	});
    	
    	$('#orgId').select2({
    		placeholder: 'Select Organization',
    		tags: true,
		    multiple: false,
		    tokenSeparators: [',', ' '],
		    minimumInputLength: 2,
		    minimumResultsForSearch: 10,
		    ajax: {
		        url: 'http://localhost:8080/api/organization-all',
		        dataType: 'json',
		        type: 'GET',
		        data: function (params) {
		            var queryParameters = {
		                term: params.term
		            }
		            return queryParameters;
		        },
		        processResults: function (data) {
		            return {
		                results: $.map(data, function (item) {
		                    return {
		                        text: item.name,
		                        id: item.id
		                    }
		                })
		            };
		        }
		    }
    	});
    	
    	$('#jobId').select2({
    		placeholder: 'Select Job Position',
    		tags: true,
		    multiple: false,
		    tokenSeparators: [',', ' '],
		    minimumInputLength: 2,
		    minimumResultsForSearch: 10,
		    ajax: {
		        url: 'http://localhost:8080/api/job-all',
		        dataType: 'json',
		        type: 'GET',
		        data: function (params) {
		            var queryParameters = {
		                term: params.term
		            }
		            return queryParameters;
		        },
		        processResults: function (data) {
		            return {
		                results: $.map(data, function (item) {
		                    return {
		                        text: item.name,
		                        id: item.id
		                    }
		                })
		            };
		        }
		    }
    	});
    	
    	$('#jobLevelId').select2({
    		placeholder: 'Select Job Level',
    		tags: true,
		    multiple: false,
		    tokenSeparators: [',', ' '],
		    minimumInputLength: 2,
		    minimumResultsForSearch: 10,
		    ajax: {
		        url: 'http://localhost:8080/api/job-level-all',
		        dataType: 'json',
		        type: 'GET',
		        data: function (params) {
		            var queryParameters = {
		                term: params.term
		            }
		            return queryParameters;
		        },
		        processResults: function (data) {
		            return {
		                results: $.map(data, function (item) {
		                    return {
		                        text: item.name,
		                        id: item.id
		                    }
		                })
		            };
		        }
		    }
		});

		$('.datepicker').datepicker({
			format : 'dd-mm-yyyy',
		});
		
		$("#idCardType").select2();
		$("#gender").select2();
		$("#maritalStatus").select2();
		$("#bloodType").select2();
		$("#religion").select2();
		$("#employmentStatus").select2();
		$("#grade").select2();
		$("#selectSchedule").select2();
		$("#approval").select2();
		$("#ptkpStatus").select2();
		$("#bankName").select2();
		$("#bpjsKf").select2();
		$("#taxConfiguration").select2();
		$("#salaryConfiguration").select2();
		$("#jhtConfiguration").select2();
		$("#employeeTaxStatus").select2();
		$("#jaminanPensiunConfiguration").select2();
		$("#nppBpjsTk").select2();
		$("#overtimeStatus").select2();
		$("#bpjsKConfiguration").select2();
		$("#paymentSchedule").select2();
		$("#prorateType").select2();
		
		$('.permanent').toggle();
		
		$('#cbExpDateIdentity').click(function() {
			$('.permanent').toggle();
			$('.expired-at').toggle();
        });
		
		$('#cbResIdAddressSameWith').click(function() {
			$('#residentialIdAddress').val($('#citizenIdAddress').val());
        });
		
		$('.contract').toggle();
		
		$('#employmentStatus').change(function() {
			if($('#employmentStatus').val() === '2'){
				$('.contract').toggle();
				$('.contract').attr('data-toggle', 'show');
			} else if($('.contract').attr('data-toggle')==='show'){
				$('.contract').toggle();
				$('.contract').attr('data-toggle', 'hide');
			}
        });
		
		$('#approval').select2({
    		placeholder: '--Select Employee--',
    		tags: true,
		    multiple: false,
		    tokenSeparators: [',', ' '],
		    minimumInputLength: 2,
		    minimumResultsForSearch: 10,
		    ajax: {
		        url: 'http://localhost:8080/api/employee-all',
		        dataType: 'json',
		        type: 'GET',
		        data: function (params) {
		            var queryParameters = {
		                term: params.term
		            }
		            return queryParameters;
		        },
		        processResults: function (data) {
		            return {
		                results: $.map(data, function (emp) {
		                    return {
		                        text: emp.id + ' - ' + emp.firstName+' '+emp.lastName,
		                        id: emp.id
		                    }
		                })
		            };
		        }
		    }
    	});
    });
	
	resetToastPosition = function() {
        $('.jq-toast-wrap').removeClass('bottom-left bottom-right top-left top-right mid-center'); // to remove previous position class
        $(".jq-toast-wrap").css({
          "top": "",
          "left": "",
          "bottom": "",
          "right": ""
        }); //to remove previous position style
    }

})(jQuery);