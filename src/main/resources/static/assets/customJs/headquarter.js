(function($) {
    'use strict';
    function notif(message) {
        resetToastPosition();
        $.toast({
            text: message,
            showHideTransition: 'slide',
            icon: 'error',
            loaderBg: '#f2a654',
            position: 'top-right'
          })
    }

    $(function() {
    	loadLogoImage();
    	loadSignatureImage();
    	
        $("#save").click(function() {
            if ($('#companyName').val() == "") {
                notif("Company name must be filled")
            } else if ($('#hqInitial').val() == "") {
                notif("HQ initial must be filled")
            } else if ($('#companyAddress').val() == "") {
                notif("Company Address must be filled")
            } else if ($('#postalCode').val() == "") {
                notif("Postal code must be filled")
            } else if ($('#umr').val() == "") {
                notif("UMR must be filled")
            } else if ($('#phoneNumber').val() == "") {
                notif("Phone number must be filled")
            } else if ($('#email').val() == "") {
                notif("Email must be filled")
            } else if ($('#companyNPWP').val() == "") {
                notif("Company NPWP must be filled")
            } else if ($('#taxableDate').val() == "") {
                notif("Taxable date must be filled")
            } else if ($('#taxPersonName').val() == "") {
                notif("Tax person name must be filled")
            } else if ($('#taxPersonNPWP').val() == "") {
                notif("Tax person NPWP must be filled")
            }

            if ($('#companyName').val().length > 100) {
                notif("Please enter company name no more than 100 characters")
            } else if ($('#hqInitial').val().length > 20) {
                notif("Please enter HQ initial no more than 20 characters")
            } else if ($('#companyAddress').val().length > 255) {
                notif("Please enter company address no more than 255 characters")
            } else if ($('#postalCode').val().length > 10) {
                notif("Please enter postal code no more than 10 characters")
            } else if ($('#umr').val().length > 15) {
                notif("Please enter umr no more than 15 characters")
            } else if ($('#phoneNumber').val().length > 14) {
                notif("Please enter phone number no more than 14 characters")
            } else if ($('#email').val().length > 50) {
                notif("Please enter email no more than 50 characters")
            } else if ($('#companyNPWP').val().length > 20) {
                notif("Please enter company NPWP no more than 20 characters")
            } else if ($('#taxPersonName').val().length > 50) {
                notif("Please enter tax person name no more than 50 characters")
            } else if ($('#taxPersonNPWP').val().length > 20) {
                notif("Please enter tax person npwp no more than 20 characters")
            }
        })

        $("#headquarterForm").validate({
            rules:{
                name: {
                    required: true,
                    maxlength: 100
                },
                hqInitial: {
                    required: true,
                    maxlength: 20
                },
                address: {
                    required: true,
                    maxlength: 255
                }, 
                postalCode: {
                    required: true,
                    maxlength: 10,
                },
                umr: {
                    required: true,
                    maxlength: 15
                },
                phone: {
                    required: true,
                    maxlength: 14
                },
                email: {
                    required: true,
                    maxlength: 50,
                },
                npwp: {
                    required: true,
                    maxlength: 20
                }, 
                taxableDate :{
                    required: true
                },
                taxPerson: {
                    required: true,
                    maxlength: 50
                },
                taxPersonNpwp: {
                    required: true,
                    maxlength: 20
                },
            },
            messages:{},
            errorPlacement: function(label, element) {
                var idE = element.attr('id');
                var errorSelector = '.validation_error_message[for="' + idE + '"]';
                var $element = $(errorSelector);
                if ($element.length) {
                    $(errorSelector).html(label.html());
                } else {
                    label.addClass('mt-2 text-danger');
                    label.insertAfter(element);
                }
            },
            highlight: function(element, errorClass) {
                $(element).parent().addClass('has-danger')
                $(element).addClass('form-control-danger')
            }
        });

        $("#companyTaxableDate").change(function() {
            if ($("#companyTaxableDate").val().length != 0) {
                $(".companyTaxableDate").hide();
            }
        });
        
    });

    $('.datepicker').datepicker({
    	format : 'dd-mm-yyyy',
    });

    resetToastPosition = function() {
        $('.jq-toast-wrap').removeClass('bottom-left bottom-right top-left top-right mid-center'); // to remove previous position class
        $(".jq-toast-wrap").css({
          "top": "",
          "left": "",
          "bottom": "",
          "right": ""
        }); //to remove previous position style
    }
    
    function loadLogoImage(){
    	var image = $('#logoImg').val();
    	if(image){
    		var imagePath = '../images/' + image;
        	var drEvent = $('#logo').dropify({
        	  defaultFile: imagePath
        	});
        	drEvent = drEvent.data('dropify');
        	drEvent.resetPreview();
        	drEvent.clearElement();
        	drEvent.settings.defaultFile = imagePath;
        	drEvent.destroy();
        	drEvent.init();
    	}
    }
    
    function loadSignatureImage(){
    	var image = $('#signatureImg').val();
    	if(image){
    		var imagePath = '../images/' + image;
        	var drEvent = $('#signature').dropify({
        	  defaultFile: imagePath
        	});
        	drEvent = drEvent.data('dropify');
        	drEvent.resetPreview();
        	drEvent.clearElement();
        	drEvent.settings.defaultFile = imagePath;
        	drEvent.destroy();
        	drEvent.init();
    	}
    }
    
  })(jQuery);