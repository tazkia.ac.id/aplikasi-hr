(function($) {
    $('.datepicker').datepicker({
        // format : 'dd MM, yyyy',
        format : 'dd-mm-yyyy',
    });

    $(function() {
    	
        $('table.display').DataTable({
            "language": {
                search: ""
            },
            "scrollX": true,
            "scrollCollapse": true,
            "fixedColumns":   {
                leftColumns: 2
            }
        })
        $('table.display').each(function() {
            var datatable = $(this);
            // SEARCH - Add the placeholder for Search and Turn this into in-line form control
            var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
            search_input.attr('placeholder', 'Search');
            search_input.removeClass('form-control-sm');
            // LENGTH - Inline-Form control
            var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
            length_sel.removeClass('form-control-sm');
        });
        
        $('#orgId').select2({
    		placeholder: '--Select Organization--',
    		tags: true,
		    multiple: false,
		    tokenSeparators: [',', ' '],
		    minimumInputLength: 2,
		    minimumResultsForSearch: 10,
		    ajax: {
		        url: 'http://localhost:8080/api/organization-all',
		        dataType: 'json',
		        type: 'GET',
		        data: function (params) {
		            var queryParameters = {
		                term: params.term
		            }
		            return queryParameters;
		        },
		        processResults: function (data) {
		            return {
		                results: $.map(data, function (item) {
		                    return {
		                        text: item.name,
		                        id: item.id
		                    }
		                })
		            };
		        }
		    }
    	});
        var $orgId = $("<option selected></option>").val($("#oldOrgId").val()).text($("#oldOrgName").val());
        $("#orgId").append($orgId).trigger('change');	//default
        
        $('#jobId').select2({
    		placeholder: '--Select Job Position--',
    		tags: true,
		    multiple: false,
		    tokenSeparators: [',', ' '],
		    minimumInputLength: 2,
		    minimumResultsForSearch: 10,
		    ajax: {
		        url: 'http://localhost:8080/api/job-all',
		        dataType: 'json',
		        type: 'GET',
		        data: function (params) {
		            var queryParameters = {
		                term: params.term
		            }
		            return queryParameters;
		        },
		        processResults: function (data) {
		            return {
		                results: $.map(data, function (item) {
		                    return {
		                        text: item.name,
		                        id: item.id
		                    }
		                })
		            };
		        }
		    }
    	});
        var $jobId = $("<option selected></option>").val($("#oldJobId").val()).text($("#oldJobName").val());
        $("#jobId").append($jobId).trigger('change');
        
        $('#jobLevelId').select2({
    		placeholder: '--Select Job Level--',
    		tags: true,
		    multiple: false,
		    tokenSeparators: [',', ' '],
		    minimumInputLength: 2,
		    minimumResultsForSearch: 10,
		    ajax: {
		        url: 'http://localhost:8080/api/job-level-all',
		        dataType: 'json',
		        type: 'GET',
		        data: function (params) {
		            var queryParameters = {
		                term: params.term
		            }
		            return queryParameters;
		        },
		        processResults: function (data) {
		            return {
		                results: $.map(data, function (item) {
		                    return {
		                        text: item.name,
		                        id: item.id
		                    }
		                })
		            };
		        }
		    }
    	});
        var $jobLvId = $("<option selected></option>").val($("#oldJobLvId").val()).text($("#oldJobLvName").val());
        $("#jobLevelId").append($jobLvId).trigger('change');
        
        $('.contract').toggle();
        
        var empStatusId = $("#employmentStatus").val();
        var $empStatus;
        switch (empStatusId) { 
	    	case '1': 
	    		$empStatus = $("<option selected></option>").val(empStatusId).text('Permanent');
	    		break;
	    	case '2': 
	    		$empStatus = $("<option selected></option>").val(empStatusId).text('Contract');
	    		break;
	    	case '3': 
	    		$empStatus = $("<option selected></option>").val(empStatusId).text('Probation');
	    		break;
	    	default:
	    		$empStatus = $("<option selected></option>").val('0').text('--Select Status Employee--');
	    }
        $("#employmentStatus").append($empStatus).trigger('change');
        
        $('#approval').select2({
    		placeholder: '--Select Employee--',
    		tags: true,
		    multiple: false,
		    tokenSeparators: [',', ' '],
		    minimumInputLength: 2,
		    minimumResultsForSearch: 10,
		    ajax: {
		        url: 'http://localhost:8080/api/employee-all',
		        dataType: 'json',
		        type: 'GET',
		        data: function (params) {
		            var queryParameters = {
		                term: params.term
		            }
		            return queryParameters;
		        },
		        processResults: function (data) {
		            return {
		                results: $.map(data, function (emp) {
		                    return {
		                        text: emp.id + ' - ' + emp.firstName+' '+emp.lastName,
		                        id: emp.id
		                    }
		                })
		            };
		        }
		    }
    	});
    });
    
    $('#employmentStatus').change(function() {
		if($('#employmentStatus').val() === '2'){
			$('.contract').toggle();
			$('.contract').attr('data-toggle', 'show');
		} else if($('.contract').attr('data-toggle')==='show'){
			$('.contract').toggle();
			$('.contract').attr('data-toggle', 'hide');
		}
    });

    $('a[data-toggle="pill"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
           .columns.adjust();
    });
})(jQuery);