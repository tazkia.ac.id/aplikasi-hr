(function ($) {
    'use strict';

    $('#jstree_sample_1').on("loaded.jstree", function (event, data) {
        $(this).jstree("open_all");
    });

    $('#jstree_sample_1').jstree({
        "types": {
            "default": {
                "icon": false
            },
            "file": {
                "icon": false
            }
        },
        "plugins": [
            "types", 
            "contextmenu"
        ]
    });
    $('#jstree_sample_2').jstree({
        "types": {
            "default": {
                "icon": "../../../assets/images/file-icons/extension/folder.png"
            },
            "file": {
                "icon": "../../../assets/images/file-icons/extension/doc-file.png",
                "valid_children": []
            }
        },
        "plugins": [
            "types",
            "checkbox"
        ]
    });
    $('#jstree_sample_3').jstree({
        "types": {
            "default": {
                "icon": false
            },
            "file": {
                "icon": false,
                "valid_children": []
            }
        },
        "core": {
            // so that create works
            "check_callback": true
        },
        "plugins": [
            "types",
            "contextmenu"
        ]
    });
    $('#jstree_sample_4').jstree({
        "types": {
            "default": {
                "icon": "../../../assets/images/file-icons/extension/folder.png"
            },
            "file": {
                "icon": "../../../assets/images/file-icons/extension/doc-file.png",
                "valid_children": []
            },
            'f-open': {
                'icon': 'fa fa-folder-open fa-fw'
            },
            'f-closed': {
                'icon': 'fa fa-folder fa-fw'
            }
        },
        "core": {
            "variant": "large",
            "check_callback": true
        },
        "renameItem" : {
            // The item label
            "label"				: "Ganti nama",
            // The function to execute upon a click
            "action"			: function (obj) { this.rename(obj); },
            // All below are optional 
            "_disabled"			: true,		// clicking the item won't do a thing
            "_class"			: "class",	// class is applied to the item LI node
            "separator_before"	: false,	// Insert a separator before the item
            "separator_after"	: true,		// Insert a separator after the item
            // false or string - if does not contain `/` - used as classname
            "icon"				: false,
            "submenu"			: { 
                /* Collection of objects (the same structure) */
            }
        },
        "plugins" : [ "themes", "html_data", "ui", "crrm", "contextmenu" ]
    });
})(jQuery);