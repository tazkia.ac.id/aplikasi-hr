INSERT INTO s_permission (id, name, description) VALUES
  ('viewcompany', 'VIEW_COMPANY', 'Lihat Data Perusahaan'),
  ('addcompany', 'ADD_COMPANY', 'Menambah Data Perusahaan'),
  ('editcompany', 'EDIT_COMPANY', 'Update Data Perusahaan'),
  ('viewemployee', 'VIEW_EMPLOYEE', 'Lihat Data Karyawan'),
  ('addemployee', 'ADD_EMPLOYEE', 'Menambah Data Karyawan'),
  ('editemployee', 'EDIT_EMPLOYEE', 'Update Data Karyawan');

INSERT INTO s_role (id, name, description) VALUES
  ('user', 'ROLE_USER', 'User'),
  ('admin', 'ADMIN', 'Admin'),
  ('super_admin', 'SUPER_ADMIN', 'Super Admin');

INSERT INTO s_role_permission (id_role, id_permission) VALUES
  ('user', 'viewcompany'),  
  ('admin', 'addcompany'),
  ('super_admin', 'addcompany'),
  ('admin', 'editcompany'),
  ('super_admin', 'editcompany'),
  ('user', 'viewemployee'),  
  ('admin', 'addemployee'),
  ('super_admin', 'addemployee'),
  ('admin', 'editemployee'),
  ('super_admin', 'editemployee');