create table s_user (
  id VARCHAR(50) NOT NULL PRIMARY KEY,
  name varchar (100) not null,
  email varchar (100) not null,
  password varchar (100),
  image_url varchar (100),
  id_role varchar (36) not null,
  employee_id VARCHAR(50),
  is_active int DEFAULT 1,
  foreign key (id_role) references s_role(id),
  foreign key (employee_id) references employee(id),
  unique (email)
);