CREATE TABLE s_role (
  id VARCHAR(255) NOT NULL,
  name VARCHAR(255) DEFAULT NULL,
  description VARCHAR(255),
  PRIMARY KEY (id),
  UNIQUE (name)
);