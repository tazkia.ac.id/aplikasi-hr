CREATE TABLE `organization` (
  `id` binary(16) NOT NULL PRIMARY KEY,
  `name` varchar(100) NOT NULL,
  `parent` binary(16) DEFAULT NULL,
  `is_active` int DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
